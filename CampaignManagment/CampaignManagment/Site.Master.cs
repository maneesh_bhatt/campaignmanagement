﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;
using System.IO;
using MySql.Data.MySqlClient;
using CustomFunctions;

namespace CampaignManagement
{
    public partial class Site : System.Web.UI.MasterPage
    {
        MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
        protected void Page_Load(object sender, EventArgs e)
        {
        
            if (!IsPostBack)
            {
 
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                {
                    Response.Redirect("LoginPage.aspx");
                }
                else
                {
                    lblUserName.Text = Convert.ToString(Session["UserID"]);
                }
                 
            }

        }
 

      
        protected void lnkLogout_OnClick(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("LoginPage.aspx");
        }


        


    
    }
}