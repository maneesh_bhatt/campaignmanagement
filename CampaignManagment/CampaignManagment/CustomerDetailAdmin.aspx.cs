﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using MySql.Data.MySqlClient;
using CampaignManagement;
using System.Data;
using System.IO;

namespace CampaignManagment
{
    public partial class CustomerDetailAdmin : System.Web.UI.Page
    {
        MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
        public static string searchCondition;
        private static bool[] rightsArray;
        Image sortImage = new Image();
        private static string sortCondition;
        private static string sortColumn;
        public string SortDirection
        {
            get
            {
                if (ViewState["SortDirection"] == null)
                    return string.Empty;
                else
                    return ViewState["SortDirection"].ToString();
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        private string _sortDirection;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("LoginPage.aspx");
            searchCondition = "";
            if (!Page.IsPostBack)
            {

                BindGrid();
            }

        }



        protected void BindGrid()
        {

            try
            {
                MySqlDataAdapter adapter = new MySqlDataAdapter();



                string query = "SELECT * FROM campaign_customer ";
                if (Convert.ToString(ViewState["SearchCondition"]) != "")
                {

                    adapter = new MySqlDataAdapter(query + "  where  " + ViewState["SearchCondition"], con);

                }
                else
                {
                    adapter = new MySqlDataAdapter(query, con);

                }
                DataTable td = new DataTable();
                adapter.Fill(td);
                //gridCampaign.DataSource = td;
                DataView view = null;
                view = new DataView(td);
                if (!string.IsNullOrEmpty(sortCondition))
                {
                    view.Sort = sortCondition;
                }
                else
                {
                    view.Sort = "Campaign_Customer_Id  Desc";
                }

                gridCampaign.AllowSorting = true;
                gridCampaign.DataSource = view;
                gridCampaign.DataBind();
                gridCampaign.EmptyDataText = "No Record Found";
                lblRecorrdsCount.Text = Convert.ToString(td.Rows.Count) + " " + "Records In Total";
                if (td.Rows.Count > 0)
                {
                    btnExcelUpload.Visible = true;
                }
                else
                {
                    btnExcelUpload.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {

            }

        }




        protected void gridCampaign_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {

            //if (e.CommandName == "Edit")
            //{

            //    Response.Redirect("CampaignForm.aspx?CampaignDetailId=" + e.CommandArgument);
            //}
            if (e.CommandName == "Delete")
            {
                try
                {
                    con.Open();
                    MySqlCommand cmd = new MySqlCommand("delete from campaign_customer where Campaign_Customer_Id=@CustomerDetailId", con);
                    cmd.Parameters.AddWithValue("@CustomerDetailId", e.CommandArgument);
                    cmd.ExecuteNonQuery();
                    BindGrid();
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
                finally
                {
                    con.Close();
                }
            }

        }


        protected void gridCampaign_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Set the index of the new display page.  
            gridCampaign.PageIndex = e.NewPageIndex;
            BindGrid();
            if (SortDirection == "ASC")
            {
                _sortDirection = "ASC";
                sortImage.ImageUrl = "Images/view_sort_descending.png";

            }
            else
            {
                _sortDirection = "DESC";
                sortImage.ImageUrl = "Images/view_sort_ascending.png";
            }
            ApplySortingImage(sortColumn);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //gridPackages.PageIndex = 1;
            searchCondition = "";
            if (inputCategory.Text != "")
            {
                searchCondition = "Category like '%" + inputCategory.Text + "%'";
            }
            //if (inputTitle.Text != "")
            //{
            //    if (searchCondition != "")
            //    {
            //        searchCondition = searchCondition + " and Title like '%" + inputTitle.Text + "%'";
            //    }
            //    else
            //    {
            //        searchCondition = " Title like '%" + inputTitle.Text + "%'";
            //    }
            //}

            ViewState["SearchCondition"] = searchCondition;
            BindGrid();
        }


        protected void gridCampaign_OnDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }

        }

        protected void gridCampaign_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<string> GetCategory(string text)
        {
            MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
            try
            {
                con.Open();
                List<string> allItems = new List<string>();
                MySqlCommand cmd = new MySqlCommand();
                if (!string.IsNullOrWhiteSpace(text))
                {
                    cmd = new MySqlCommand("select distinct Category from campaign_detail where Category like '%" + text + "%' order by Category ASC", con);
                }
                else
                {
                    cmd = new MySqlCommand("select distinct Category from campaign_detail order by Category ASC", con);
                }
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string productName = Convert.ToString(reader["Category"]);
                    allItems.Add(productName);
                }
                reader.Close();
                return allItems;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return null;
            }
            finally
            {
                con.Close();
            }
        }

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public static List<string> GetTitle(string text, string Category)
        //{
        //    MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
        //    try
        //    {
        //        con.Open();
        //        List<string> allItems = new List<string>();
        //        MySqlCommand cmd = new MySqlCommand();
        //        string searchquery = "";
        //        if (!string.IsNullOrEmpty(Category))
        //        {
        //            searchquery = "Category= '" + Category + "'";
        //        }
        //        if (searchquery != "")
        //        {
        //            if (!string.IsNullOrWhiteSpace(text))
        //            {
        //                cmd = new MySqlCommand("select distinct Title from campaign_detail where Title like '%" + text + "%' and " + searchquery + " order by Title ASC", con);
        //            }
        //            else
        //            {
        //                cmd = new MySqlCommand("select distinct Title from campaign_detail where  " + searchquery + " order by Title ASC", con);
        //            }
        //        }
        //        else
        //        {
        //            if (!string.IsNullOrWhiteSpace(text))
        //            {
        //                cmd = new MySqlCommand("select distinct Title from campaign_detail where Title like '%" + text + "%'  order by Title ASC", con);
        //            }
        //            else
        //            {
        //                cmd = new MySqlCommand("select distinct Title from campaign_detail  order by Title ASC", con);
        //            }
        //        }
        //        MySqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            string documentName = Convert.ToString(reader["Title"]);
        //            allItems.Add(documentName);
        //        }
        //        reader.Close();
        //        return allItems;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        return null;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}



        protected void gridCampaign_Sorting(object sender, GridViewSortEventArgs e)
        {
            // gridBagList.PageIndex = 0;
            SetSortDirection(SortDirection);
            sortCondition = e.SortExpression + " " + _sortDirection;
            sortColumn = e.SortExpression;
            BindGrid();
            SortDirection = _sortDirection;
            ApplySortingImage(e.SortExpression);

        }


        protected void ApplySortingImage(string sortExpression)
        {
            int columnIndex = 0;
            foreach (DataControlFieldHeaderCell headerCell in gridCampaign.HeaderRow.Cells)
            {
                if (headerCell.ContainingField.SortExpression == sortExpression)
                {
                    columnIndex = gridCampaign.HeaderRow.Cells.GetCellIndex(headerCell);
                }
            }

            gridCampaign.HeaderRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void SetSortDirection(string sortDirection)
        {
            if (sortDirection == "ASC")
            {
                _sortDirection = "DESC";
                sortImage.ImageUrl = "Images/view_sort_ascending.png";

            }
            else
            {
                _sortDirection = "ASC";
                sortImage.ImageUrl = "Images/view_sort_descending.png";
            }
        }
        protected void btnExcelUpload_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "Customer Details" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            gridCampaign.AllowPaging = false;
            this.BindGrid();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + "");
            gridCampaign.GridLines = GridLines.Both;
            gridCampaign.HeaderStyle.Font.Bold = true;
            gridCampaign.RenderControl(htmltextwrtter);
            Response.Write("<style> TABLE { border:dotted 1px #999; } " +
                "TD { border:dotted 1px #D5D5D5; text-align:center } </style>");

            Response.Write(strwritter.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}