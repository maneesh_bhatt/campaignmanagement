﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CampaignForm.aspx.cs" Inherits="CampaignManagement.ProductDocumentForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <div id="content" class="span10">
        <asp:HiddenField runat="server" ID="hdnVersionNumber" ClientIDMode="Static" />
        <ul class="breadcrumb">
            <li><i class="icon-home"></i><a href="#">Home</a> <i class="icon-angle-right"></i>
            </li>
            <li><i class="icon-edit"></i><a href="#">Campaign Form</a> </li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>Campaign Form</h2>
                    <div class="box-icon" runat="server" id="viewAll">
                        <asp:HyperLink ID="HyperLink1" runat="server" Style="color: White;" NavigateUrl="CampaignAdmin.aspx">View All</asp:HyperLink>
                    </div>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label></p>
                            <div class="control-group">
                                <label class="control-label">
                                    Campaign Name <span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateCampaign">
                                        <ContentTemplate>
                                            <asp:TextBox runat="server" ID="inputCategory" ValidationGroup="Document" CssClass="textboxAuto"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Document"
                                                runat="server" ControlToValidate="inputCategory" ErrorMessage="Category is required."
                                                ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:Label ID="lblCategory" runat="server" ForeColor="Red"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Title 
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ID="inputTitle" ValidationGroup="Document" CssClass="textboxAuto"></asp:TextBox>
                                    
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Top Description
                                </label>
                                <div class="controls" style = "margin-left:250px!important;">
                                    <asp:TextBox runat="server" ValidationGroup="Document" TextMode="MultiLine" Rows="4"
                                        CssClass="cleditor" ID="txtTopDescription" ></asp:TextBox>
                                </div>
                                
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Description 
                                </label>
                                <div class="controls" style = "margin-left:250px!important;">
                                    <asp:TextBox runat="server" ValidationGroup="Document" ID="inputDescription" CssClass="cleditor"
                                        TextMode="MultiLine" Rows="4"></asp:TextBox>
                                    
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Top Bannner (1033px * 350px)
                                </label>
                                <div class="controls">
                                    <asp:FileUpload runat="server" ValidationGroup="Document" ID="inputTopBanner" />
                                    <%--<div class="controls">--%>
                                    <asp:CheckBox runat="server" ValidationGroup="Document" 
                                        ID="checkIsActiveTopBanner"></asp:CheckBox>
                                    Is Active
                                <%--</div>--%>
                                </div>
                                
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Left Banner (512px * 350px)
                                </label>
                                <div class="controls">
                                    <asp:FileUpload runat="server" ValidationGroup="Document" ID="inputLeftBanner" />
                                    
                                    <asp:CheckBox runat="server" ValidationGroup="Document" 
                                        ID="checkIsActiveLeftBanner"></asp:CheckBox>
                                    Is Active
                                
                                </div>
                                
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                   Redirect After Submit (If Download Link is Inactive)
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" ID="inputRemarks"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    DownLoad Link
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" ID="inputDownLoadLink"></asp:TextBox>
                                    
                                    <asp:CheckBox runat="server" ValidationGroup="Document" 
                                        ID="checkIsActiveDownloadLink"></asp:CheckBox>
                                    Is Active
                                
                                </div>
                                 
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Email Subject
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtEmailSubject" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Email Body
                                </label>
                                <div class="controls" style = "margin-left:250px!important;">
                                    <asp:TextBox runat="server" ValidationGroup="Document" TextMode="MultiLine" Rows="4"
                                        CssClass="cleditor" ID="txtEmailBody"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Form Name
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" 
                                        CssClass="cleditor" ID="txtFormName"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Telephone Number
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" 
                                        CssClass="cleditor" ID="txtTelephoneNumber"></asp:TextBox>
                                </div>
                                
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label">
                                    LinkedIn
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" 
                                        CssClass="cleditor" ID="txtLinkedIn"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    YouTube
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" 
                                        CssClass="cleditor" ID="txtYouTube"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Twitter
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" 
                                        CssClass="cleditor" ID="txtTwitter"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    FaceBook
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" 
                                        CssClass="cleditor" ID="txtFaceBook"></asp:TextBox>
                                </div>
                            </div>
                              <div class="control-group">
                                <label class="control-label">
                                    Text Below Contact Form
                                </label>
                                <div class="controls" style = "margin-left:250px!important;">
                                    <asp:TextBox runat="server" ValidationGroup="Document" TextMode="MultiLine" Rows="4"
                                        CssClass="cleditor" ID="inputTextBelowContentForm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Link Formed
                                </label>
                                <div class="controls">
                                    <asp:Label ID="lblLinkFormed" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="form-actions">
                                <asp:LinkButton runat="server" ID="btnSave" ValidationGroup="Document" CausesValidation="true"
                                    class="btn btn-primary" Text="Save changes" OnClick="btnSave_Click">
                                </asp:LinkButton>
                                <asp:Button runat="server" ID="btnClear" ValidationGroup="Document" Text="Clear"
                                    class="btn" OnClick="btnClear_Click"></asp:Button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
    </div>
    <%--<script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    LoadDocumentType();
                    LoadProductName();
                }
            });
        };
    </script>--%>
</asp:Content>
