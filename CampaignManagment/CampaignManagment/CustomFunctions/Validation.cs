﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Web.Services;
using System.Web.Script.Services;
using CampaignManagement;
using System.Web.UI.WebControls;

namespace CustomFunctions
{
    public static class Validation
    {
        public static bool ValidateDataExist(string tableName, string columnName, string value)
        {
            bool recordExist = false;
            MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand("select " + columnName + " from " + tableName + " where " + columnName + "='" + value + "'", con);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        recordExist = true;
                    }
                    else
                    {
                        recordExist = false;
                    }
                }
                return recordExist;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return recordExist;
            }
            finally
            {
                con.Close();
            }
        }


        public static string RetrieveIdByText(string tableName, string textColumnName, string idColumnName, string textValue)
        {
            string idColumnValue = "";
            MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand("select " + idColumnName + " from " + tableName + " where " + textColumnName + "='" + textValue + "'", con);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        idColumnValue = Convert.ToString(reader[idColumnName]);
                    }
                }
                return idColumnValue;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return idColumnValue;
            }
            finally
            {
                con.Close();
            }
        }


        public static string RetrieveTextById(string tableName, string textColumnName, string idColumnName, string idValue)
        {
            string textColumnValue = "";
            MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand("select " + textColumnName + " from " + tableName + " where " + idColumnName + "='" + idValue + "'", con);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        textColumnValue = Convert.ToString(reader[textColumnName]);
                    }
                }
                return textColumnValue;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return textColumnValue;
            }
            finally
            {
                con.Close();
            }
        }

        public static Dictionary<string, string> RetrieveMultipleColumnsByCondition(string tableName, string textColumns, string whereCondition)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand("select " + textColumns + " from " + tableName + " where " + whereCondition + "", con);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        string[] columns = textColumns.Split(',');
                        for (int i = 0; i < columns.Length; i++)
                        {
                            string column = Convert.ToString(columns[i]).Trim();
                            dictionary.Add(Convert.ToString(column), Convert.ToString(reader[column]));
                        }
                    }
                }
                return dictionary;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return null;
            }
            finally
            {
                con.Close();
            }
        }


        public static string RetrieveColumnByCondition(string tableName, string columnName, string whereCondition)
        {
            string textColumnValue = "";
            MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand("select " + columnName + " from " + tableName + " where " + whereCondition, con);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        textColumnValue = Convert.ToString(reader[columnName]);
                    }
                }
                return textColumnValue;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return textColumnValue;
            }
            finally
            {
                con.Close();
            }
        }


        //public static void ApproveSelectedRecords(GridView gridview, string tableName, string idColumnName, string idColumnValue)
        //{

        //    MySqlConnection connec = new MySqlConnection(InitializeConnection.connection);
        //    MySqlTransaction trans = null;
        //    try
        //    {
        //        connec.Open();
        //        trans = connec.BeginTransaction();
        //        MySqlCommand cmd = new MySqlCommand();
        //        cmd.Connection = connec;
        //        cmd.Transaction = trans;
        //        foreach (GridViewRow gr in gridview.Rows)
        //        {

        //            CheckBox checkedRecords = (CheckBox)gridview.Rows[gr.RowIndex].FindControl("checkRecords");
        //            HiddenField hdnTicketDetailId = (HiddenField)gridview.Rows[gr.RowIndex].FindControl("Id");
        //            if (checkedRecords.Checked)
        //            {
        //                cmd.Parameters.Clear();
        //                cmd.CommandText = "update " + tableName + " set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy where "+idColumnName+"=@Id";
        //                cmd.Parameters.AddWithValue("@IsApproved", true);
        //                cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
        //                cmd.Parameters.AddWithValue("@UpdatedBy", HttpContext.Current.Session["UserID"]);
        //                cmd.Parameters.AddWithValue("@Id", hdnTicketDetailId.Value);
        //                cmd.ExecuteNonQuery();

        //            }
        //        }
        //        trans.Commit();
        //        //lblMessage.Text = "Record Approved Successfully.";
        //        //BindGrid();
        //    }
        //    catch (Exception ex)
        //    {
        //        trans.Rollback();
        //        // ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Some error occured while saving the data.');", true);
        //        ex.ToString();
        //    }
        //    finally
        //    {
        //        connec.Close();
        //    }

        //}


        public static string ApproveDisApproveRecords(string type, GridView gridview, string tableName, string idColumnName, string sendApprovalTo)
        {
            string message = "";
            MySqlConnection connec = new MySqlConnection(InitializeConnection.connection);
            MySqlTransaction trans = null;
            MySqlDataReader reader;
            try
            {
                connec.Open();
                trans = connec.BeginTransaction();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = connec;
                cmd.Transaction = trans;
                foreach (GridViewRow gr in gridview.Rows)
                {

                    CheckBox checkedRecords = (CheckBox)gridview.Rows[gr.RowIndex].FindControl("checkRecords");
                    HiddenField hdnId = (HiddenField)gridview.Rows[gr.RowIndex].FindControl("hdnId");
                    TextBox txtRemarks = (TextBox)gridview.Rows[gr.RowIndex].FindControl("txtRemarks");
                    if (checkedRecords.Checked)
                    {
                        cmd.Parameters.Clear();
                        if (type == "Send For Approval")
                        {
                            cmd.CommandText = "update " + tableName + " set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approval_Sent_To=@SendApprovalTo,Approval_Request_Date=@ApprovalRequestDate where " + idColumnName + "=@Id";
                            cmd.Parameters.AddWithValue("@SendApprovalTo", sendApprovalTo);
                            cmd.Parameters.AddWithValue("@ApprovalRequestDate", DateTime.Now);
                        }
                        else if (type == "Approved")
                        {
                            cmd.CommandText = "update " + tableName + " set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approved_By=@ApprovedBy, Approval_Date=@ApprovalDate where " + idColumnName + "=@Id";
                            cmd.Parameters.AddWithValue("@ApprovedBy", HttpContext.Current.Session["UserID"]);
                            cmd.Parameters.AddWithValue("@ApprovalDate", DateTime.Now);
                        }
                        else if (type == "Disapproved")
                        {
                            cmd.CommandText = "update " + tableName + " set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approved_By=NULL, Approval_Date=NULL where " + idColumnName + "=@Id";
                        }
                        cmd.Parameters.AddWithValue("@RecordStatus", type);
                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@UpdatedBy", HttpContext.Current.Session["UserID"]);
                        cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                        cmd.ExecuteNonQuery();
                        if (type == "Approved" || type == "Disapproved")
                        {
                            if (txtRemarks != null)
                            {
                                cmd.Parameters.Clear();
                                cmd.CommandText = "insert into Remarks_Master(Remarks,Table_Name,Table_Name_Id,Created_By,Created_Date) values('" + txtRemarks .Text+ "','" + tableName + "'," + hdnId.Value + ",@createdBy,@CreatedDate)";
                                cmd.Parameters.AddWithValue("@createdBy", HttpContext.Current.Session["UserID"]);
                                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                                cmd.ExecuteNonQuery();
                            }
                        }
                        if (type == "Send For Approval")
                        {

                            if (tableName == "Problem_Logging")
                            {
                                string preventiveMaintenanceId = "";
                                string CheckPointId = "";
                                string AuditPointId = "";
                                string AlarmToBeGeneratedId = "";
                                string DesignModificationId = "";
                                string ProcedureAmendmentId = "";
                                string TrainingRequirementId = "";
                                cmd.Parameters.Clear();
                                cmd.CommandText = "select * from Problem_Logging where Problem_Logging_Id=@ProblemLoggingId";
                                cmd.Parameters.AddWithValue("@ProblemLoggingId", hdnId.Value);
                                reader = cmd.ExecuteReader();
                                if (reader.HasRows)
                                {
                                    reader.Read();
                                    preventiveMaintenanceId = Convert.ToString(reader["Preventive_Maintenance_Id"]);
                                    CheckPointId = Convert.ToString(reader["Check_Point_ID"]);
                                    AuditPointId = Convert.ToString(reader["Audit_Point_ID"]);
                                    AlarmToBeGeneratedId = Convert.ToString(reader["Alarm_Needs_To_Be_Generated_ID"]);
                                    DesignModificationId = Convert.ToString(reader["Design_Modification_ID"]);
                                    ProcedureAmendmentId = Convert.ToString(reader["Procedure_Amendment_ID"]);
                                    TrainingRequirementId = Convert.ToString(reader["Training_Requirement_ID"]);
                                }
                                reader.Close();

                                if (!string.IsNullOrEmpty(preventiveMaintenanceId))
                                {
                                    string Record_Status = "";
                                    cmd.CommandText = "select * from Preventive_Maintenance where Preventive_Maintenance_Id = @PreventiveMaintenanceId";
                                    cmd.Parameters.AddWithValue("@PreventiveMaintenanceId", preventiveMaintenanceId);
                                    reader = cmd.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        Record_Status = Convert.ToString(reader["Record_Status"]);
                                    }
                                    reader.Close();
                                    if (Record_Status != "Approved")
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "update Preventive_Maintenance set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approval_Sent_To=@SendApprovalTo,Approval_Request_Date=@ApprovalRequestDate where Preventive_Maintenance_Id=@PreventiveMaintenanceId";
                                        cmd.Parameters.AddWithValue("@PreventiveMaintenanceId", preventiveMaintenanceId);
                                        cmd.Parameters.AddWithValue("@SendApprovalTo", sendApprovalTo);
                                        cmd.Parameters.AddWithValue("@RecordStatus", type);
                                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@UpdatedBy", HttpContext.Current.Session["UserID"]);
                                        cmd.Parameters.AddWithValue("@ApprovalRequestDate", DateTime.Now);
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                                if (!string.IsNullOrEmpty(CheckPointId))
                                {
                                    string Record_Status = "";
                                    cmd.CommandText = "select * from check_point where Check_Point_ID = @CheckPointID";
                                    cmd.Parameters.AddWithValue("@CheckPointID", CheckPointId);
                                    reader = cmd.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        Record_Status = Convert.ToString(reader["Record_Status"]);
                                    }
                                    reader.Close();
                                    if (Record_Status != "Approved")
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "update check_point set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approval_Sent_To=@SendApprovalTo,Approval_Request_Date=@ApprovalRequestDate where Check_Point_ID=@CheckPointID";
                                        cmd.Parameters.AddWithValue("@CheckPointID", CheckPointId);
                                        cmd.Parameters.AddWithValue("@SendApprovalTo", sendApprovalTo);
                                        cmd.Parameters.AddWithValue("@RecordStatus", type);
                                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@UpdatedBy", HttpContext.Current.Session["UserID"]);
                                        cmd.Parameters.AddWithValue("@ApprovalRequestDate", DateTime.Now);

                                        cmd.ExecuteNonQuery();
                                    }
                                }
                                if (!string.IsNullOrEmpty(AuditPointId))
                                {
                                    string Record_Status = "";
                                    cmd.CommandText = "select * from audit_point where Audit_Point_ID = @AuditPointID";
                                    cmd.Parameters.AddWithValue("@AuditPointID", AuditPointId);
                                    reader = cmd.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        Record_Status = Convert.ToString(reader["Record_Status"]);
                                    }
                                    reader.Close();
                                    if (Record_Status != "Approved")
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "update audit_point set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approval_Sent_To=@SendApprovalTo,Approval_Request_Date=@ApprovalRequestDate where Audit_Point_ID=@AuditPointID";
                                        cmd.Parameters.AddWithValue("@AuditPointID", AuditPointId);
                                        cmd.Parameters.AddWithValue("@SendApprovalTo", sendApprovalTo);
                                        cmd.Parameters.AddWithValue("@RecordStatus", type);
                                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@UpdatedBy", HttpContext.Current.Session["UserID"]);
                                        cmd.Parameters.AddWithValue("@ApprovalRequestDate", DateTime.Now);
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                                if (!string.IsNullOrEmpty(AlarmToBeGeneratedId))
                                {
                                    string Record_Status = "";
                                    cmd.CommandText = "select * from alarm_needs_to_be_generated where Alarm_Needs_To_Be_Generated_ID = @AlarmNeedsToBeGeneratedID";
                                    cmd.Parameters.AddWithValue("@AlarmNeedsToBeGeneratedID", AlarmToBeGeneratedId);
                                    reader = cmd.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        Record_Status = Convert.ToString(reader["Record_Status"]);
                                    }
                                    reader.Close();
                                    if (Record_Status != "Approved")
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "update alarm_needs_to_be_generated set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approval_Sent_To=@SendApprovalTo,Approval_Request_Date=@ApprovalRequestDate where Alarm_Needs_To_Be_Generated_ID=@AlarmNeedsToBeGeneratedID";
                                        cmd.Parameters.AddWithValue("@AlarmNeedsToBeGeneratedID", AlarmToBeGeneratedId);
                                        cmd.Parameters.AddWithValue("@SendApprovalTo", sendApprovalTo);
                                        cmd.Parameters.AddWithValue("@RecordStatus", type);
                                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@UpdatedBy", HttpContext.Current.Session["UserID"]);
                                        cmd.Parameters.AddWithValue("@ApprovalRequestDate", DateTime.Now);
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                                if (!string.IsNullOrEmpty(DesignModificationId))
                                {
                                    string Record_Status = "";
                                    cmd.CommandText = "select * from design_modification where Design_Modification_ID = @DesignModificationID";
                                    cmd.Parameters.AddWithValue("@DesignModificationID", DesignModificationId);
                                    reader = cmd.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        Record_Status = Convert.ToString(reader["Record_Status"]);
                                    }
                                    reader.Close();
                                    if (Record_Status != "Approved")
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "update design_modification set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approval_Sent_To=@SendApprovalTo,Approval_Request_Date=@ApprovalRequestDate where Design_Modification_ID=@DesignModificationID";
                                        cmd.Parameters.AddWithValue("@DesignModificationID", DesignModificationId);
                                        cmd.Parameters.AddWithValue("@SendApprovalTo", sendApprovalTo);
                                        cmd.Parameters.AddWithValue("@RecordStatus", type);
                                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@UpdatedBy", HttpContext.Current.Session["UserID"]);
                                        cmd.Parameters.AddWithValue("@ApprovalRequestDate", DateTime.Now);
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                                if (!string.IsNullOrEmpty(ProcedureAmendmentId))
                                {
                                    string Record_Status = "";
                                    cmd.CommandText = "select * from procedure_amendment where Procedure_Amendment_ID = @ProcedureAmendmentID";
                                    cmd.Parameters.AddWithValue("@ProcedureAmendmentID", ProcedureAmendmentId);
                                    reader = cmd.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        Record_Status = Convert.ToString(reader["Record_Status"]);
                                    }
                                    reader.Close();
                                    if (Record_Status != "Approved")
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "update procedure_amendment set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approval_Sent_To=@SendApprovalTo,Approval_Request_Date=@ApprovalRequestDate where Procedure_Amendment_ID=@ProcedureAmendmentID";
                                        cmd.Parameters.AddWithValue("@ProcedureAmendmentID", ProcedureAmendmentId);
                                        cmd.Parameters.AddWithValue("@SendApprovalTo", sendApprovalTo);
                                        cmd.Parameters.AddWithValue("@RecordStatus", type);
                                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@UpdatedBy", HttpContext.Current.Session["UserID"]);
                                        cmd.Parameters.AddWithValue("@ApprovalRequestDate", DateTime.Now);
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                                if (!string.IsNullOrEmpty(TrainingRequirementId))
                                {
                                    string Record_Status = "";
                                    cmd.CommandText = "select * from training_requirement where Training_Requirement_ID = @TrainingRequirementID";
                                    cmd.Parameters.AddWithValue("@TrainingRequirementID", TrainingRequirementId);
                                    reader = cmd.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        Record_Status = Convert.ToString(reader["Record_Status"]);
                                    }
                                    reader.Close();
                                    if (Record_Status != "Approved")
                                    {
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = "update training_requirement set Record_Status=@RecordStatus, Updated_Date=@UpdatedDate, Updated_By=@UpdatedBy, Approval_Sent_To=@SendApprovalTo,Approval_Request_Date=@ApprovalRequestDate where Training_Requirement_ID=@TrainingRequirementID";
                                        cmd.Parameters.AddWithValue("@TrainingRequirementID", TrainingRequirementId);
                                        cmd.Parameters.AddWithValue("@SendApprovalTo", sendApprovalTo);
                                        cmd.Parameters.AddWithValue("@RecordStatus", type);
                                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                                        cmd.Parameters.AddWithValue("@UpdatedBy", HttpContext.Current.Session["UserID"]);
                                        cmd.Parameters.AddWithValue("@ApprovalRequestDate", DateTime.Now);
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                        }

                    }
                }
                trans.Commit();
                message = "success";
                return message;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                message = "error";
                return message;
                ex.ToString();
            }
            finally
            {
                connec.Close();
            }
        }

        public static string ApprovarCondition(string type, string alias, string userType)
        {

            string displayCondition = "";
            if (type == "ShowAll")
            {
                if (userType == "admin")
                {
                    displayCondition = "1=1";
                }
                else
                {
                    if (alias != "")
                    {
                        displayCondition = "((" + alias + ".Created_By='" + HttpContext.Current.Session["UserID"] + "' or " + alias + ".Approval_Sent_To='" + HttpContext.Current.Session["UserID"] + "' or " + alias + ".Approved_By='" + HttpContext.Current.Session["UserID"] + "') or " + alias + ".Record_Status='Approved')";
                    }
                    else
                    {
                        displayCondition = "((Created_By='" + HttpContext.Current.Session["UserID"] + "' or Approval_Sent_To='" + HttpContext.Current.Session["UserID"] + "' or Approved_By='" + HttpContext.Current.Session["UserID"] + "') or Record_Status='Approved')";
                    }
                }
            }
            else if (type == "SendForApproval")
            {
                if (userType == "admin")
                {
                    if (alias != "")
                    {
                        displayCondition = "(" + alias + ".Record_Status='Send For Approval')";
                    }
                    else
                    {
                        displayCondition = "(Record_Status='Send For Approval')";
                    }
                }
                else
                {
                    if (alias != "")
                    {
                        displayCondition = "((" + alias + ".Approval_Sent_To='" + HttpContext.Current.Session["UserID"] + "' and " + alias + ".Record_Status='Send For Approval') or (" + alias + ".Created_By='" + HttpContext.Current.Session["UserID"] + "' and " + alias + ".Record_Status='Send For Approval'))";
                    }
                    else
                    {
                        displayCondition = "((Approval_Sent_To='" + HttpContext.Current.Session["UserID"] + "' and Record_Status='Send For Approval') or (Created_By='" + HttpContext.Current.Session["UserID"] + "' and Record_Status='Send For Approval'))";
                    }
                }
            }
            else if (type == "ShowApprovedRecords")
            {
                if (userType == "admin")
                {
                    if (alias != "")
                    {
                        displayCondition = "" + alias + ".Record_Status='Approved'";
                    }
                    else
                    {
                        displayCondition = "Record_Status='Approved'";
                    }
                }
                else
                {
                    if (alias != "")
                    {
                        displayCondition = "(" + alias + ".Record_Status='Approved' and " + alias + ".Created_By='" + HttpContext.Current.Session["UserID"] + "')";
                    }
                    else
                    {
                        displayCondition = "(Record_Status='Approved' and Created_By='" + HttpContext.Current.Session["UserID"] + "')";
                    }
                }
            }
            else if (type == "ShowPendingForSubmission")
            {
                if (userType == "admin")
                {
                    if (alias != "")
                    {
                        displayCondition = "(" + alias + ".Record_Status is null or " + alias + ".Record_Status='' or " + alias + ".Record_Status='Disapproved')";
                    }
                    else
                    {
                        displayCondition = "(Record_Status is null or Record_Status='' or Record_Status='Disapproved')";
                    }
                }
                else
                {
                    if (alias != "")
                    {
                        displayCondition = "(" + alias + ".Record_Status is null or " + alias + ".Record_Status='' or " + alias + ".Record_Status='Disapproved') and " + alias + ".Created_By='" + HttpContext.Current.Session["UserID"] + "' ";
                    }
                    else
                    {
                        displayCondition = "(Record_Status is null or Record_Status='' or Record_Status='Disapproved') and Created_By='" + HttpContext.Current.Session["UserID"] + "' ";
                    }
                }
            }
            return displayCondition;
        }


        public static void HideShowControlsByApprovar(RadioButton radioShowAll, RadioButton radioShowSendForApproval, RadioButton radioShowApprovedRecords, RadioButton radioShowpendingForSubmission,
            CheckBox checkRecords, CheckBox checkWorkAsAdmin,
            Button btnApproveRecords, Button btnDissapprove, Button btnSendForApproval,
            DropDownList inputSendApprovalTo, string createdBy, string approvalSentTo, TextBox txtRemarks)
        {

            if (radioShowAll.Checked)
            {
                checkRecords.Visible = false;
                btnApproveRecords.Visible = false;
                btnDissapprove.Visible = false;
                btnSendForApproval.Visible = false;
                inputSendApprovalTo.Visible = false;
            }
            else if (radioShowSendForApproval.Checked)
            {
                btnSendForApproval.Visible = false;
                inputSendApprovalTo.Visible = false;
                if (createdBy == Convert.ToString(HttpContext.Current.Session["UserID"]) &&  !checkWorkAsAdmin.Checked)
                {
                    checkRecords.Visible = false;
                    btnApproveRecords.Visible = false;
                    btnDissapprove.Visible = false;
                }
                else if (Convert.ToBoolean(HttpContext.Current.Session["IsAdministrator"])==true && checkWorkAsAdmin.Checked)
                {
                    checkRecords.Visible = true;
                    btnApproveRecords.Visible = true;
                    btnDissapprove.Visible = true;
                    txtRemarks.Enabled = true;
                }
                else if (approvalSentTo == Convert.ToString(HttpContext.Current.Session["UserID"]))
                {
                    checkRecords.Visible = true;
                    btnApproveRecords.Visible = true;
                    btnDissapprove.Visible = true;
                    txtRemarks.Enabled = true;
                }

            }
            else if (radioShowApprovedRecords.Checked)
            {

                btnDissapprove.Visible = false;
                btnSendForApproval.Visible = false;
                inputSendApprovalTo.Visible = false;
                btnApproveRecords.Visible = false;

                if (createdBy == Convert.ToString(HttpContext.Current.Session["UserID"]) && !checkWorkAsAdmin.Checked)
                {
                    checkRecords.Visible = false;
                }
                else if (Convert.ToBoolean(HttpContext.Current.Session["IsAdministrator"]) == true && checkWorkAsAdmin.Checked)
                {
                    checkRecords.Visible = true;
                    btnDissapprove.Visible = true;
                    txtRemarks.Enabled = true;
                }
                else if (approvalSentTo == Convert.ToString(HttpContext.Current.Session["UserID"]))
                {
                    checkRecords.Visible = false;
                }
            }
            else if (radioShowpendingForSubmission.Checked)
            {
                btnApproveRecords.Visible = false;
                btnDissapprove.Visible = false;
                if (createdBy == Convert.ToString(HttpContext.Current.Session["UserID"]))
                {
                    checkRecords.Visible = true;
                    btnSendForApproval.Visible = true;
                    inputSendApprovalTo.Visible = true;
                }
                else if (Convert.ToBoolean(HttpContext.Current.Session["IsAdministrator"]) == true && checkWorkAsAdmin.Checked)
                {
                    checkRecords.Visible = true;
                    btnSendForApproval.Visible = true;
                    inputSendApprovalTo.Visible = true;
                }
                else if (approvalSentTo == Convert.ToString(HttpContext.Current.Session["UserID"]))
                {
                    checkRecords.Visible = false;
                    btnSendForApproval.Visible = false;
                    inputSendApprovalTo.Visible = false;
                }
            }
        }
    }
}