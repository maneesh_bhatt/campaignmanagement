﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Campaign.aspx.cs" Inherits="CampaignManagement.Campaign" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="basic.css" rel="stylesheet" type="text/css" />
    <title>Campaign</title>
    <style type="text/css">
        body
        {
            line-height: 26px;
            font-family: Roboto;
        }
        /* @font-face
        {
            font-family: 'RobotoNormal';
            src: url('font/Roboto/Roboto-Black.ttf');
        }
        @font-face
        {
            font-family: 'RobotoItalic';
            src: url('font/Roboto/Roboto-BlackItalic.ttf');
        }
        @font-face
        {
            font-family: 'RobotoBold';
            src: url('font/Roboto/Roboto-Bold.ttf');
        }
        @font-face
        {
            font-family: 'RobotoLight';
            src: url('font/Roboto/Roboto-Light.ttf');
        }
        @font-face
        {
            font-family: 'RobotoMedium';
            src: url('font/Roboto/Roboto-Medium.ttf');
        }*/
        @font-face
        {
            font-family: 'Roboto';
            src: url('font/Roboto/Roboto-Regular.ttf');
        }
        .main_content
        {
            width: 85%;
            margin: auto; /*border: 1px solid #cacaca;
            box-shadow: 0px 2px 3px #cacaca;*/
            height: auto;
        }
        .enquiry-form
        {
            border: 1px solid #cacaca;
            padding: 10px;
            width: 100%;
        }
        .cotrols-container
        {
            height: 50px;
        }
        .content-title
        {
            color: #079ae2;
            font-size: 22px;
            margin-bottom: 6px;
        }
        .SubmitButton
        {
            background-color: #079ae2;
            color: white;
            border: 1px solid #dadada;
            padding: 6px;
        }
        .updateprogess-img
        {
            width: 14%;
            float: left;
            margin-top: -39px;
            margin-left: 65px;
        }
        .validationmessage
        {
            font-size: 13px;
        }
    </style>
    <meta name="robots" content="noindex,nofollow" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <div class="main_content">
        <div style="padding: 10px;">
            <a target="_blank" href="http://www.falconautoonline.com">
                <img src="Images/falconautotechlogo.png" style="margin-bottom: 10px;" /></a>
            <div id="ContactNumber" runat="server" class="contactno" style="text-align: right;">
                <img src="img/TelephoneIcon.png" style="margin-bottom: -10px;" />
                <asp:Label ID="lblContactNumber" runat="server"></asp:Label></div>
            <div>
                <asp:Image ID="ImgTopBanner" runat="server" class="topbanner" Visible="false" />
            </div>
            <div id="TopDecsription" runat="server">
                <asp:Label ID="lblTopDescription" runat="server"></asp:Label></div>
            <div class="center-section">
                <div class="center-containers-first">
                    <div class="content-title">
                        <asp:Label runat="server" ID="lblTitle" Style="margin-bottom: 15px!important;"></asp:Label></div>
                    <asp:Image ID="ImgLeftBanner" runat="server" class="leftbanner" Visible="false" Style="margin-bottom: 25px!important;" />
                    <asp:Label runat="server" ID="lblDescription"></asp:Label>
                </div>
                <div class="center-containers-second" style="margin-bottom: 15px!important;">
                    <table class="enquiry-form">
                        <tr>
                            <td colspan="2">
                                <span class="content-title">
                                    <asp:Label ID="lblFormName" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblMessage" runat="server" ForeColor="green"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:LinkButton runat="server" ID="lnkbtnDownload" Visible="false" Text="Click here to Download"></asp:LinkButton>
                                <!--OnClick="lnkbtnDownload_Click" -->
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td style="width: 35%;">
                                Full Name<span style="color: Red;">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtName" CssClass="controls"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFullName" Display="Dynamic"
                                    ControlToValidate="txtName" ErrorMessage="Name is required" ForeColor="Red" CssClass="validationmessage" ValidationGroup="Enquiry"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td style="width: 35%;">
                                Email Address<span style="color: Red;">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="controls"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" CssClass="validationmessage" ID="RequiredFieldValidator1" Display="Dynamic"
                                    ControlToValidate="txtEmail" ErrorMessage="Email ID is required" ForeColor="Red"
                                    ValidationGroup="Enquiry"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" CssClass="validationmessage" Display="Dynamic"
                                    runat="server" ErrorMessage="Enter Valid Email ID" ControlToValidate="txtEmail"
                                    ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="Enquiry"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td style="width: 35%;">
                                Contact Number
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtMobileNumber" CssClass="controls" MaxLength="20"
                                    ValidationGroup="Enquiry"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td style="width: 35%;">
                                Company<span style="color: Red;">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtCompany" CssClass="controls" ValidationGroup="Enquiry"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" CssClass="validationmessage" ID="RequiredFieldValidator2" Display="Dynamic"
                                    ControlToValidate="txtCompany" ErrorMessage="Company is required" ForeColor="Red"
                                    ValidationGroup="Enquiry"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td style="width: 35%;">
                                Comments
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtComments" CssClass="controls" Style="height: 100px;"
                                    TextMode="MultiLine" Rows="6" ValidationGroup="Enquiry"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updateSubmit" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Submit"
                                            CssClass="SubmitButton" ValidationGroup="Enquiry"></asp:Button>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="updateSubmit">
                                    <ProgressTemplate>
                                        <img src="Images/loading.gif" class="updateprogess-img" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                    <div style="margin-top: 20px; padding: 10px; width: 95%;">
                        <asp:Label runat="server" ID="lblContactPersonInfo"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer" style="margin-top: 25px!important;">
            <span class="toplink">Falcon Autotech Pvt Ltd Copyright &copy;<%=DateTime.Now.Year %></span>
            <div id="ImgBtns" runat="server" class="toplink" style="text-align: center;">
                <asp:ImageButton ID="ImagBtnLinkedId" runat="server" ImageUrl="img/linkedin-24x24.png"
                    OnClick="ImagBtn_Click" CommandName="LinkedIn" OnClientClick="form1.target ='_blank';" />&nbsp;
                &nbsp;
                <asp:ImageButton ID="ImgBtnYouTube" runat="server" ImageUrl="img/YouTube-icon.png"
                    OnClick="ImagBtn_Click" CommandName="Youtube" OnClientClick="form1.target ='_blank';" />&nbsp;
                &nbsp;
                <asp:ImageButton ID="ImgBtnTwitter" runat="server" ImageUrl="img/twitter-24x24.png"
                    OnClick="ImagBtn_Click" CommandName="Twitter" OnClientClick="form1.target ='_blank';" />&nbsp;
                &nbsp;
                <asp:ImageButton ID="ImgBtnFaceBook" runat="server" ImageUrl="img/facebook-24x24.png"
                    OnClick="ImagBtn_Click" CommandName="Facebook" OnClientClick="form1.target ='_blank';" />
            </div>
            <ul class="menulinks" style="list-style: none; width: 30%;">
                <%--display: flex;--%>
                <%--  <li><a target="_blank" href="https://www.falconautoonline.com/about-us/" style="text-decoration: none;
                    color: white;">About Us&nbsp;&nbsp;&nbsp;&nbsp; </a></li>
                <li><a target="_blank" href="https://www.falconautoonline.com/falcon-products-solutions/"
                    style="text-decoration: none; color: white;">Products&nbsp;&nbsp;&nbsp;&nbsp; </a>
                </li>
                <li><a target="_blank" href="https://www.falconautoonline.com/contact/" style="text-decoration: none;
                    color: white;">Contact Us&nbsp;&nbsp;&nbsp;&nbsp; </a></li>--%>
                <li style="text-align: right;"><a target="_blank" href="https://www.falconautoonline.com/privacy-policy/"
                    style="text-decoration: none; color: white;">Privacy Policy </a></li>
            </ul>
        </div>
    </div>
    </form>
</body>
</html>
