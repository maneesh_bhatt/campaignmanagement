﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Dashboard.aspx.cs" Inherits="FCM.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .web_dialog
        {
            display: none;
            position: fixed;
            height: 616px;
            left: 6%;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
            top: 3%;
            background-color: #FFF;
            border: 2px solid #369;
            width: 1200px;
        }
        
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
            height: 26px;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .gridcontainer
        {
            margin-right: 7px;
            margin-top: 20px;
            margin-left: 7px;
        }
        
        
        .containerhead
        {
            background-color: #4583C7;
            padding: 10px;
            font-weight: normal;
            border: 1px solid #dadada;
            color: white;
            font-size: 17px;
        }
        .anchor, .anchor:hover
        {
            background-color: #4292a3;
            color: white;
            padding: 32px;
            font-size: 17px;
            box-shadow: 2px 1px 1px #4292a3;
            width: 19.9% !important;
            float: left;
            margin: 2%;
            text-align: center;
            margin-top: 28px;
            text-decoration: none;
            height: 60px;
        }
        .button-container
        {
            height: 80px;
            text-align: center;
            width: 28%;
            margin: auto;
            width: 44%;
        }
        .mainhead
        {
            text-align: center;
            font-size: 17px;
            color: white;
            margin-top: 1px;
            background-color: #34afd1;
            padding: 11px;
            font-weight: normal;
        }
        
        .navbox
        {
            background-color: #F9F9F9;
            margin: auto;
            width: 100% !important;
            border: 1px solid #DDC7C7;
            box-shadow: 0px 1px 1px #676464;
            height: 437px;
        }
        .Shipmentshead
        {
            color: #4292a3;
            font-size: 16px;
            text-decoration: underline;
            margin-left: auto;
            margin-right: auto;
            width: 100%;
            text-align: left;
        }
        .searchcontrols
        {
            margin-top: 10px;
            margin-left: auto;
            margin-right: auto;
            width: 90%;
        }
        .displayfilter
        {
            border: 1px solid #4583C7 !important;
            width: 270px;
            height: 28PX;
            margin-top: 10px;
        }
        .closepopup
        {
            float: right;
            width: 50px;
            margin-top: -3px;
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">


        function DisplayInFeedChart() {
            $("#overlay").show();
            $("#dialogInFeed").fadeIn(300);
            e.preventDefault();
        }

        function DisplayShipmentScanned() {
            $("#overlay").show();
            $("#dialogInScanned").fadeIn(300);
            e.preventDefault();
        }
        function DisplayShipmentSorted() {
            $("#overlay").show();
            $("#dialogSorted").fadeIn(300);
            e.preventDefault();
        }




        function ClosePopUp() {

            HideDialog();
            e.preventDefault();
        }


        function HideDialog() {
            $("#overlay").hide();
            $(".web_dialog").fadeOut(300);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <div id="content" class="span10">
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Dashboard.aspx">Home</a> <i class="icon-angle-right">
            </i></li>
            <li><a href="Dashboard.aspx">Dashboard</a></li>
        </ul>
        <asp:Label runat="server" ID="lblMessage" ForeColor="Red"></asp:Label>
    </div>
</asp:Content>
