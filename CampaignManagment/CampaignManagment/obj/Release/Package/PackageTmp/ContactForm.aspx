﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactForm.aspx.cs" Inherits="CampaignManagment.ContactForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="basic.css" rel="stylesheet" type="text/css" />
    <title></title>
    <style type="text/css">
        .main_content
        {
            width: 80%;
            margin: auto;
            border: 1px solid #cacaca;
            box-shadow: 0px 2px 3px #cacaca;
            height: auto;
        }
        .enquiry-form
        {
            border: 1px solid #cacaca;
            padding: 10px;
            width: 100%;
        }
        .cotrols-container
        {
            height: 50px;
        }
        .content-title
        {
            color: #079ae2;
            font-size: 22px;
            margin-bottom: 6px;
        }
        .SubmitButton
        {
            background-color: #079ae2;
            color: white;
            border: 1px solid #dadada;
            padding: 6px;
        }
    </style>
 

</head>
<body>
    <form id="form1" runat="server">
   <%-- <div class="main_content">
        <div style="padding: 10px;">
            
            <div class="center-section">--%>
                
                <div class="center-containers-second">
                    <table class="enquiry-form" style="width:75%;">
                        <tr>
                            <td>
                                <span class="content-title">Enquiry Form</span>
                            </td>
                            <td>
                                <asp:Label ID="lblMessage" runat="server" ForeColor="green"></asp:Label>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td>
                                Full Name<span style="color: Red;">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtName" CssClass="controls"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFullName" Display="Dynamic"
                                    ControlToValidate="txtName" ErrorMessage="Name is required" ForeColor="Red" ValidationGroup = "Enquiry"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td>
                                Email Address<span style="color: Red;">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="controls"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" Display="Dynamic"
                                    ControlToValidate="txtEmail" ErrorMessage="Email ID is required" ForeColor="Red" ValidationGroup = "Enquiry"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"
                                    runat="server" ErrorMessage="Enter Valid Email ID" ControlToValidate="txtEmail"
                                    ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup = "Enquiry"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td>
                                Contact Number
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtMobileNumber" CssClass="controls" MaxLength = "20" ValidationGroup = "Enquiry"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td>
                                Company
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtCompany" CssClass="controls" ValidationGroup = "Enquiry"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td>
                                Comments
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtComments" CssClass="controls" Style="height: 100px;"
                                    TextMode="MultiLine" Rows="6" ValidationGroup = "Enquiry"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="cotrols-container">
                            <td>
                            </td>
                            <td>
                                <asp:Button runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Submit"
                                    CssClass="SubmitButton" ValidationGroup = "Enquiry"></asp:Button>
                                    
                            </td>
                        </tr>
                    </table>
                </div>
           <%-- </div>
        </div>
        
           
    </div>--%>
    </form>
</body>
</html>
