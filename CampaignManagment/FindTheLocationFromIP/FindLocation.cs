﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Runtime.InteropServices;
using MySql.Data.MySqlClient;
using System.IO;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;



namespace FindTheLocationFromIP
{
    partial class FindLocation : ServiceBase
    {
        public FindLocation()
        {
            InitializeComponent();
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);
        //private System.ComponentModel.IContainer components;
        System.Timers.Timer timer;
        Boolean itsRunning = false;

        MySqlConnection con = new MySqlConnection();
        protected override void OnStart(string[] args)
        {
            // Update the service state to Start Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Set up a timer to trigger every minute.
            timer = new System.Timers.Timer();
            timer.Interval = 60000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();

            // Update the service state to Running.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected override void OnStop()
        {
            timer.Stop();

        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            if (!itsRunning)
            {
                itsRunning = true;
                try
                {
                    string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ConfigInfo.txt");
                    StreamReader reader = new StreamReader(filePath);
                    string content = reader.ReadToEnd();
                    string[] getUserCredentials = content.Split(';');
                    if (getUserCredentials[0] != null)
                    {
                        MySqlConnectionStringBuilder connectionstring = new MySqlConnectionStringBuilder();
                        connectionstring.Server = "192.168.1.60";
                        connectionstring.Database = "campaignmanagement";
                        connectionstring.MinimumPoolSize = 50;
                        connectionstring.MaximumPoolSize = 1000;
                        connectionstring.ConnectionTimeout = 60;
                        connectionstring.UserID = getUserCredentials[0];
                        // connectionstring.Password = getUserCredentials[1];
                        con.ConnectionString = connectionstring.ToString();
                    }
                    string cityName = string.Empty;
                    string countryName = string.Empty;
                    con.Open();
                    MySqlConnection connection = new MySqlConnection(con.ConnectionString);
                    MySqlCommand cmd = new MySqlCommand("select IP_Address from campaign_customer where City = '' and Country = ''", con);
                    MySqlDataReader reader1 = cmd.ExecuteReader();
                    if (reader1.HasRows)
                    {
                        connection.Open();
                        while (reader1.Read())
                        {
                            if (Convert.ToString(reader1["IP_Address"]) != "")
                            {
                                string url = "http://freegeoip.net/json/" + Convert.ToString(reader1["IP_Address"]);
                                WebClient client = new WebClient();
                                string jsonstring = client.DownloadString(url);
                                dynamic dynObj = JsonConvert.DeserializeObject(jsonstring);
                                cityName = dynObj.city;
                                countryName = dynObj.country_name;
                                MySqlCommand cmd1 = new MySqlCommand("update campaign_customer set City = '" + cityName + "',Country = '" + countryName + "' where IP_Address = @IPAddress", connection);
                                cmd1.Parameters.AddWithValue("@IPAddress", Convert.ToString(reader1["IP_Address"]));
                                cmd1.ExecuteNonQuery();
                            }
                        }
                        connection.Close();
                    }
                    reader1.Close();
                    }
                catch (Exception ex)
                {
                    File.WriteAllText(@"D:\\error.txt", ex.Message);
                }
                finally
                {
                    con.Close();
                    itsRunning = false;
                }
            }
        }

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long dwServiceType;
            public ServiceState dwCurrentState;
            public long dwControlsAccepted;
            public long dwWin32ExitCode;
            public long dwServiceSpecificExitCode;
            public long dwCheckPoint;
            public long dwWaitHint;
        };
    }
}
