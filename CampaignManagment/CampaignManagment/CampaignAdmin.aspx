﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CampaignAdmin.aspx.cs" Inherits="CampaignManagement.ProductDocumentAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 780px;
            height: 490px;
            top: 25%;
            left: 42%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
        .web_dialog_remarks
        {
            display: none;
            position: fixed;
            width: 700px;
            height: 490px;
            top: 28%;
            left: 30%;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
        .awbimagecontainer
        {
            float: right;
            width: 29%;
            margin-right: 12px;
            margin-top: 43px;
            border: 1px solid #4583C7;
            height: 180px;
        }
    </style>
    <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
     

//        function CheckButton(){

//            if (!$(this).hasClass("aspNetDisabled")) {
//                alert($(this).hasClass("aspNetDisabled"));
//                var confirmed = confirm('Are you sure you want to delete this item?');
//                if (!confirmed) {
//                    return false;
//                }

//               }
//               
//        }

        $(function () {
            $('#<%=inputCategory.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "CampaignAdmin.aspx/GetCategory",
                        data: "{ 'text':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                $(this).autocomplete("search", "");
            });
        });


        $(function () {
            $('#<%=inputTitle.ClientID%>').autocomplete({
                source: function (request, response) {
                    var Category = $('#<%=inputCategory.ClientID %>').val();
                    $.ajax({
                        url: "CampaignAdmin.aspx/GetTitle",
                        data: "{ 'text':'" + request.term + "','Category':'" + Category + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return { value: item }
                            }))
                        }
                    });
                },
                minLength: 0,
                scroll: true
            }).focus(function () {
                $(this).autocomplete("search", "");
            });
        });
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li><i class="icon-home"></i><a href="Dashboard.aspx">Home</a> <i class="icon-angle-right">
            </i></li>
            <li><a href="#">Dashboard</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon align-justify"></i>Campaign Admin</h2>
                    <div class="box-icon" runat="server" id="btnAddNew">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="CampaignForm.aspx" Style="color: White;">Add New</asp:HyperLink>
                    </div>
                </div>
                <div class="box-content">
                    <asp:TextBox runat="server" ID="inputCategory" Style="width: 200px;" Placeholder="Campaign Name"
                        CssClass="textboxAuto"></asp:TextBox>&nbsp;
                    <asp:TextBox runat="server" ID="inputTitle" Style="width: 200px;" Placeholder="Title"
                        CssClass="textboxAuto"></asp:TextBox>&nbsp;
                    <asp:Button runat="server" ID="btnSearch" class="btn btn-primary" Style="margin-top: -10px;"
                        Text="Search" OnClick="btnSearch_Click" />
                    <table class="table table-bordered table-striped table-condensed">
                        <asp:GridView runat="server" ID="gridCampaign" BackColor="White" AutoGenerateColumns="false"
                            EmptyDataText="No Records Found" ShowHeaderWhenEmpty="True" AllowPaging="true"
                            AllowSorting="false" BorderColor="#B8BABD" BorderStyle="None" OnPageIndexChanging="gridCampaign_PageIndexChanging"
                            PageSize="15" BorderWidth="1px" OnRowDataBound="gridCampaign_OnDataBound" OnRowCommand="gridCampaign_OnRowCommand"
                            Width="100%" OnRowDeleting="gridCampaign_RowDeleting" OnSorting="gridCampaign_Sorting">
                            <HeaderStyle CssClass="tableheader" />
                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                            <Columns>
                                <asp:BoundField DataField="Category" HeaderText="Campaign Name" HeaderStyle-Width="290px"
                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#4583C7" HeaderStyle-ForeColor="White"
                                    SortExpression="Category" />
                                <asp:BoundField DataField="Title" HeaderText="Title" HeaderStyle-Width="290px" HeaderStyle-Font-Bold="false"
                                    HeaderStyle-BackColor="#4583C7" HeaderStyle-ForeColor="White" SortExpression="Title" />
                                <%--<asp:BoundField DataField="Category" HeaderText="Category" HeaderStyle-Width="290px"
                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#4583C7" HeaderStyle-ForeColor="White"
                                    SortExpression="Category" />--%>
                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" HeaderStyle-Width="290px"
                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#4583C7" HeaderStyle-ForeColor="White"
                                    SortExpression="Remarks" />
                                <asp:BoundField DataField="DownLoad_Link" HeaderText="DownLoad Link" HeaderStyle-Width="290px"
                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#4583C7" HeaderStyle-ForeColor="White"
                                    SortExpression="DownLoad_Link" />
                                <%--<asp:BoundField DataField="Updated_By" HeaderText="Updated By" HeaderStyle-Width="290px"
                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#4583C7" HeaderStyle-ForeColor="White"
                                    SortExpression="Updated By" />--%>
                                <%--<asp:BoundField DataField="Category" HeaderText="Category" HeaderStyle-Width="290px"
                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#4583C7" HeaderStyle-ForeColor="White"
                                    SortExpression="Category" />--%>
                                <asp:TemplateField HeaderText="Setting" HeaderStyle-Width="180px" HeaderStyle-Font-Bold="false"
                                    HeaderStyle-BackColor="#4583C7" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnId" ClientIDMode="Static" Value='<%#Eval("Campaign_detail_id") %>' />
                                        <asp:LinkButton runat="server" CommandName="Edit" CssClass="btnCustom" CommandArgument='<%#Eval("Campaign_detail_id") %>'
                                            ID="lnkBtnEdit">Edit</asp:LinkButton>&nbsp;&nbsp;
                                        <asp:LinkButton runat="server" CommandName="Delete" CssClass="btnCustom" OnClientClick="return Confirm();"
                                            CommandArgument='<%#Eval("Campaign_detail_id") %>' ID="lnkBtnDelete">Delete</asp:LinkButton>
                                       <%-- <asp:LinkButton runat="server" CommandName="Freeze" CssClass="btnCustom" CommandArgument='<%#Eval("Campaign_detail_id") %>'
                                            ID="lnkBtnFreeze">Freeze</asp:LinkButton>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle Font-Bold="false" />
                            <PagerStyle BackColor="#4583C7" ForeColor="Black" Font-Overline="true" Font-Strikeout="true"
                                Font-Underline="true" Font-Size="Large" HorizontalAlign="Center" CssClass="gridview" />
                            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#000" />
                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                        </asp:GridView>
                        <asp:Label runat="server" ID="lblRecorrdsCount" CssClass="recordsstatus" Width="850px"></asp:Label>
                    </table>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
</asp:Content>
