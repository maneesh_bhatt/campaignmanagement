﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;
using System.Drawing;

namespace FCM
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["UserID"] == null)
                    Response.Redirect("LoginPage.aspx");
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["NoRights"])))
                {
                    lblMessage.Text = "You don't have access rights to access " + Convert.ToString(Request.QueryString["NoRights"])+ " Page";
                }
            }
        }
    }
}