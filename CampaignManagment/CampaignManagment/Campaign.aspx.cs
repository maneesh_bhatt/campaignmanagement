﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Net;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;

namespace CampaignManagement
{
    public partial class Campaign : System.Web.UI.Page
    {
        public static MySqlConnection con = new MySqlConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                StreamReader rdr = new StreamReader(Server.MapPath("~/ConfigInfo.txt"));
                string content = rdr.ReadToEnd();
                string[] getUserCredentials = content.Split(';');
                if (getUserCredentials[0] != null)
                {
                    MySqlConnectionStringBuilder connectionstring = new MySqlConnectionStringBuilder();
                    connectionstring.Server = getUserCredentials[0];
                    connectionstring.Database = getUserCredentials[1];
                    connectionstring.MinimumPoolSize = 50;
                    connectionstring.MaximumPoolSize = 1000;
                    connectionstring.ConnectionTimeout = 60;
                    connectionstring.UserID = getUserCredentials[2];
                    if (getUserCredentials[3] != null)
                    {
                        connectionstring.Password = getUserCredentials[3];
                    }
                    con.ConnectionString = connectionstring.ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Camp"]))
                {
                    try
                    {
                        con.Open();
                        MySqlCommand cmd = new MySqlCommand("select * from campaign_detail where Category = @Category", con);
                        cmd.Parameters.AddWithValue("@Category", Convert.ToString(Request.QueryString["Camp"]));
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            lblTitle.Text = Convert.ToString(reader["Title"]);
                            if (reader["Text_Below_Contact"] != DBNull.Value)
                            {
                                lblContactPersonInfo.Text = Convert.ToString(reader["Text_Below_Contact"]);
                            }
                            if (Convert.ToBoolean(reader["Is_Active_Download_Link"]))
                            {
                                string filePath = Convert.ToString(reader["DownLoad_Link"]);
                                lnkbtnDownload.PostBackUrl = filePath;
                            }
                            if (Convert.ToBoolean(reader["Is_Active_Top_Banner"]))
                            {
                                ImgTopBanner.Visible = true;
                                ImgTopBanner.ImageUrl = Convert.ToString(reader["Top_Bannner"]);
                            }
                            else
                            {
                                ImgTopBanner.Visible = false;
                            }
                            if (Convert.ToBoolean(reader["Is_Active_Left_Banner"]))
                            {
                                ImgLeftBanner.Visible = true;
                                ImgLeftBanner.ImageUrl = Convert.ToString(reader["Left_Banner"]);
                            }
                            else
                            {
                                ImgLeftBanner.Visible = false;
                            }
                            lblDescription.Text = Convert.ToString(reader["Description"]);
                            lblFormName.Text = Convert.ToString(reader["Form_Name"]);
                            if (!String.IsNullOrEmpty(Convert.ToString(reader["Top_Description"])))
                            {
                                lblTopDescription.Text = Convert.ToString(reader["Top_Description"]);
                            }
                            else
                            {
                                TopDecsription.Visible = false;
                            }
                            if (String.IsNullOrEmpty(Convert.ToString(reader["LinkedIn"])) && String.IsNullOrEmpty(Convert.ToString(reader["YouTube"])) && String.IsNullOrEmpty(Convert.ToString(reader["Twitter"])) && String.IsNullOrEmpty(Convert.ToString(reader["FaceBook"])))
                            {
                                ImgBtns.Visible = false;
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Convert.ToString(reader["LinkedIn"])))
                                {
                                    // ImagBtnLinkedId.PostBackUrl = Convert.ToString(reader["LinkedIn"]);

                                }
                                else
                                {
                                    ImagBtnLinkedId.Visible = false;
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(reader["YouTube"])))
                                {
                                    // ImgBtnYouTube.PostBackUrl = Convert.ToString(reader["YouTube"]);
                                }
                                else
                                {
                                    ImgBtnYouTube.Visible = false;
                                }
                                if (!String.IsNullOrEmpty(Convert.ToString(reader["Twitter"])))
                                {
                                    // ImgBtnTwitter.PostBackUrl = Convert.ToString(reader["Twitter"]);
                                }
                                else
                                {
                                    ImgBtnTwitter.Visible = false;
                                }
                                if (!String.IsNullOrEmpty(Convert.ToString(reader["FaceBook"])))
                                {
                                    // ImgBtnFaceBook.PostBackUrl = Convert.ToString(reader["FaceBook"]);
                                }
                                else
                                {
                                    ImgBtnFaceBook.Visible = false;
                                }
                            }
                            if (!String.IsNullOrEmpty(Convert.ToString(reader["Telephone_Number"])))
                            {
                                lblContactNumber.Text = Convert.ToString(reader["Telephone_Number"]);
                            }
                            else
                            {
                                ContactNumber.Visible = false;
                            }
                        }
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                    finally
                    {
                        con.Close();
                    }

                }
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "ClearMessage()", true);
            lblMessage.Text = "";
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                btnSubmit.Enabled = false;
                string ipaddress = string.Empty;
                string cityName = string.Empty;
                string countryName = string.Empty;
                try
                {
                    ipaddress = Server.HtmlEncode(Request.UserHostAddress);
                    if (ipaddress != "")
                    {
                        string url = "http://freegeoip.net/json/" + ipaddress.ToString();
                        WebClient client = new WebClient();
                        string jsonstring = client.DownloadString(url);
                        dynamic dynObj = JsonConvert.DeserializeObject(jsonstring);
                        cityName = dynObj.city;
                        countryName = dynObj.country_name;
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                con.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "insert into campaign_customer(Category,Name ,Email ,Mobile_Number ,Company ,Comments ,IP_Address ,City ,Country, Created_Date) values(@Category,@Name ,@Email ,@MobileNumber ,@Company ,@Comments ,@IPAddress ,@City ,@Country ,@CreatedDate)";
                cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@MobileNumber", txtMobileNumber.Text.Trim());
                cmd.Parameters.AddWithValue("@Company", txtCompany.Text.Trim());
                cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                cmd.Parameters.AddWithValue("@Category", Request.QueryString["Camp"]);
                cmd.Parameters.AddWithValue("@IPAddress", ipaddress);
                cmd.Parameters.AddWithValue("@City", cityName);
                cmd.Parameters.AddWithValue("@Country", countryName);
                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                cmd.ExecuteNonQuery();


                SendEmail(txtEmail.Text);
                MessageAndClearControls();
                MySqlConnection conne = new MySqlConnection(con.ConnectionString);
                conne.Open();
                MySqlCommand cmd1 = new MySqlCommand("select Is_Active_Download_Link,Remarks from campaign_detail where Category=@Category", conne);
                cmd1.Parameters.AddWithValue("@Category", Convert.ToString(Request.QueryString["Camp"]));
                using (MySqlDataReader rdr = cmd1.ExecuteReader())
                {
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        bool IsActiveDownloadLink = Convert.ToBoolean(rdr["Is_Active_Download_Link"]);
                        if (IsActiveDownloadLink == true)
                        {
                            lnkbtnDownload.Visible = true;
                        }
                        else
                        {
                            lnkbtnDownload.Visible = false;
                            string redirectLink = Convert.ToString(rdr["Remarks"]);
                            if (!string.IsNullOrEmpty(redirectLink))
                            {
                                Response.Redirect(redirectLink, false);
                            }
                        }
                    }
                }
                conne.Close();

            }
            catch (Exception ex)
            {
                string error = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "    " + ex.ToString();
                if (!File.Exists(Server.MapPath("~/CampaignErrors.txt")))
                {
                    File.Create(Server.MapPath("~/CampaignErrors.txt"));
                }
                File.AppendAllText(Server.MapPath("~/CampaignErrors.txt"), error);
                ex.ToString();
            }
            finally
            {
                con.Close();

            }
        }


        //protected void lnkbtnDownload_Click(object sender, EventArgs e)
        //{
        //    MySqlConnection connec = new MySqlConnection(con.ConnectionString);
        //    try
        //    {
        //        connec.Open();
        //        MySqlCommand cmd = new MySqlCommand();
        //        cmd.Connection = connec;
        //        cmd.Parameters.Clear();
        //        cmd.CommandText = "select * from campaign_detail where Category = @Category";
        //        cmd.Parameters.AddWithValue("@Category", Convert.ToString(Request.QueryString["Camp"]));
        //        using (MySqlDataReader reader = cmd.ExecuteReader())
        //        {
        //            if (reader.HasRows)
        //            {
        //                reader.Read();
        //                if (Convert.ToBoolean(reader["Is_Active_Download_Link"]))
        //                {
        //                    string filePath = Convert.ToString(reader["DownLoad_Link"]);
        //                    lnkbtnDownload.PostBackUrl = filePath;
        //                    using (WebClient req = new WebClient())
        //                    {
        //                        HttpResponse response = HttpContext.Current.Response;
        //                        ServicePointManager.Expect100Continue = true;
        //                        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
        //                        response.Clear();
        //                        response.ClearContent();
        //                        response.ClearHeaders();
        //                        response.Buffer = true;
        //                        response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
        //                        byte[] data = req.DownloadData(filePath);
        //                        response.BinaryWrite(data);
        //                        req.Dispose();
        //                    }
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }
        //    finally
        //    {
        //        connec.Close();
        //    }
        //}
        protected void SendEmail(string VisitorEmailId)
        {
            MySqlConnection connection = new MySqlConnection(con.ConnectionString);
            try
            {

                connection.Open();
                MySqlCommand cmd = new MySqlCommand("select Email_Subject,Email_Body from campaign_detail where Category = @Category", con);
                cmd.Parameters.AddWithValue("@Category", Convert.ToString(Request.QueryString["Camp"]));
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                    MailMessage newmsg = new MailMessage();
                    newmsg.From = new MailAddress("marketing@falconautoonline.com");
                    newmsg.To.Add(VisitorEmailId);
                    newmsg.Subject = Convert.ToString(reader["Email_Subject"]);
                    string content = "";
                    if (Convert.ToString(reader["Email_Body"]) != "")
                    {
                        content = Convert.ToString(reader["Email_Body"]);
                    }
                    newmsg.Body = content;

                    SmtpServer.TargetName = "STARTTLS/smtp.office365.com";
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.Port = 587;
                    newmsg.IsBodyHtml = true;
                    SmtpServer.EnableSsl = true;
                    //Add this line to bypass the certificate validation
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
                            System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                            System.Security.Cryptography.X509Certificates.X509Chain chain,
                            System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };
                    SmtpServer.Credentials = new System.Net.NetworkCredential("marketing@falconautoonline.com", "Falcon@FAPL@2016");
                    SmtpServer.Send(newmsg);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                string error = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "    " + ex.ToString();
                if (!File.Exists(Server.MapPath("~/CampaignErrors.txt")))
                {
                    File.Create(Server.MapPath("~/CampaignErrors.txt"));
                }
                File.AppendAllText(Server.MapPath("~/CampaignErrors.txt"), error);
                ex.ToString();
            }
            finally
            {
                connection.Close();
            }
        }
        protected void MessageAndClearControls()
        {
            lblMessage.Text = "Data submitted successfully";
            txtName.Text = "";
            txtEmail.Text = "";
            txtMobileNumber.Text = "";
            txtCompany.Text = "";
            txtComments.Text = "";
        }

        protected void ImagBtn_Click(object sender, EventArgs e)
        {

            try
            {
                ImageButton imgbtn = (ImageButton)sender;
                con.Open();
                MySqlCommand cmd = new MySqlCommand("select * from campaign_detail where Category = @Category", con);
                cmd.Parameters.AddWithValue("@Category", Convert.ToString(Request.QueryString["Camp"]));
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();

                    if (imgbtn.CommandName.ToLower() == "linkedin")
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(reader["LinkedIn"])))
                        {
                            Response.Redirect(Convert.ToString(reader["LinkedIn"]));
                        }
                        else
                        {
                            ImagBtnLinkedId.Visible = false;
                        }
                    }
                    else if (imgbtn.CommandName.ToLower() == "facebook")
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(reader["Facebook"])))
                        {
                            Response.Redirect(Convert.ToString(reader["Facebook"]));
                        }
                        else
                        {
                            ImgBtnFaceBook.Visible = false;
                        }
                    }

                    else if (imgbtn.CommandName.ToLower() == "twitter")
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(reader["Twitter"])))
                        {
                            Response.Redirect(Convert.ToString(reader["Twitter"]));
                        }
                        else
                        {
                            ImgBtnTwitter.Visible = false;
                        }
                    }
                    else if (imgbtn.CommandName.ToLower() == "youtube")
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(reader["Youtube"])))
                        {
                            Response.Redirect(Convert.ToString(reader["Youtube"]));
                        }
                        else
                        {
                            ImgBtnYouTube.Visible = false;
                        }
                    }

                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }

        }
    }
}