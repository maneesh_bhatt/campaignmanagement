﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="CampaignManagement.LoginPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
    <title>Login </title>
    <style type="text/css">
        @import url(http://fonts.googleapis.com/css?family=Exo:100,200,400);
        @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);
        body
        {
            margin: 0;
            padding: 0;
            background: #fff;
            color: #fff;
            font-family: Arial;
            font-size: 12px;
        }
        .body
        {
            position: absolute;
            top: -20px;
            left: -20px;
            right: -40px;
            bottom: -40px;
            width: auto;
            height: auto; /* background-image: url('Images/city-skyline-wallpapers-008.jpg');*/
            background-size: cover;
            -webkit-filter: blur(5px);
            z-index: 0;
        }
        .grad
        {
            position: absolute;
            top: -20px;
            left: -20px;
            right: 4px;
            bottom: 234px;
            width: auto;
            height: auto; /* Chrome,Safari4+ */
            z-index: 1;
            opacity: 0.7;
        }
        .header
        {
            position: absolute;
            top: calc(50% - 35px);
            left: calc(50% - 255px);
            z-index: 2;
        }
        .header div
        {
            float: left;
            color: #fff;
            font-family: 'Exo' , sans-serif;
            font-size: 35px;
            font-weight: 200;
        }
        .header div span
        {
            color: #5379fa !important;
        }
        
        .login1
        {
            margin-top: 10px;
            float: left;
        }
        
        .login
        {
            height: 320px;
            width: 264px;
            background-color: #3c3e8d;
            padding: 70px;
            margin-top: 6%;
            margin-left: auto;
            margin-right: auto;
            border: 3px solid #3F51B5;
        }
        .login input[type=text]
        {
            width: 250px;
            height: 30px;
            background: transparent;
            border: 1px solid rgba(255,255,255,0.6);
            border-radius: 2px;
            color: #fff;
            font-family: 'Exo' , sans-serif;
            font-size: 16px;
            font-weight: 400;
            padding: 4px;
        }
        .login input[type=password]
        {
            width: 250px;
            height: 30px;
            background: transparent;
            border: 1px solid rgba(255,255,255,0.6);
            border-radius: 2px;
            color: #fff;
            font-family: 'Exo' , sans-serif;
            font-size: 16px;
            font-weight: 400;
            padding: 4px;
            margin-top: 10px;
        }
        .login input[type=button]
        {
            width: 260px;
            height: 35px;
            background: #fff;
            border: 1px solid #fff;
            cursor: pointer;
            border-radius: 2px;
            color: #a18d6c;
            font-family: 'Exo' , sans-serif;
            font-size: 16px;
            font-weight: 400;
            padding: 6px;
            margin-top: 10px;
        }
        .login-head
        {
            width: 300px;
            font-weight: normal;
            margin-top: -42px;
            font-size: 30px;
            margin-left: -15px;
            font-family: serif;
        }
        .login input[type=button]:hover
        {
            opacity: 0.8;
        }
        .login input[type=button]:active
        {
            opacity: 0.6;
        }
        .login input[type=text]:focus
        {
            outline: none;
            border: 1px solid rgba(255,255,255,0.9);
        }
        .login input[type=password]:focus
        {
            outline: none;
            border: 1px solid rgba(255,255,255,0.9);
        }
        .login input[type=button]:focus
        {
            outline: none;
        }
        ::-webkit-input-placeholder
        {
            color: rgba(255,255,255,0.6);
        }
        ::-moz-input-placeholder
        {
            color: rgba(255,255,255,0.6);
        }
    </style>
    <script type="text/javascript" src="js/prefixfree.min.js"></script>
</head>
<body>
    <form id="form1" class="form-horizontal" runat="server">
    <%--  <div class="body">
    </div>--%>
    <div class="login">
        <h1 class="login-head">
            Campaign Management</h1>
        <asp:Image ID="Image2" ImageUrl="img/falconlogo.jpg" runat="server" Style="width: 77%;
            margin-left: 9%; margin-bottom: 20px;" />
        <div style="font-size: 15px; height: 30px; float: left;">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </div>
        <div class="control">
            <asp:TextBox ID="inputUserID" placeholder="username" CssClass="login1" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="inputUserID"
                Style="color: red; visibility: visible; float: left; margin-top: 5px; margin-bottom: -5px;"
                ErrorMessage="Enter User Name" ForeColor="White"></asp:RequiredFieldValidator>
        </div>
        <div class="control">
            <asp:TextBox ID="inputPassword" CssClass="login1" placeholder="password" TextMode="Password"
                runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="inputPassword"
                Style="color: red; visibility: visible; float: left; margin-top: 5px; margin-bottom: -5px;"
                ErrorMessage="Enter Password" ForeColor="White"></asp:RequiredFieldValidator>
        </div>
        <div class="control">
            <asp:Button ID="btnLogin" CssClass="login1" runat="server" Text="Login" Width="260px"
                OnClick="btnLogin_Click" BackColor="#FFFFFF" BorderWidth="1px" BorderStyle="solid"
                BorderColor="#FFFFFF" Height="35px" ForeColor="#333333" Font-Size="16" Font-Names="'Exo',sans-serif" />
        </div>
    </div>
    </form>
</body>
</html>
