﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.IO;
using CustomFunctions;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Web.Script.Services;
using System.Drawing;

namespace CampaignManagement
{
    public partial class ProductDocumentForm : System.Web.UI.Page
    {
        MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
        static string TopBannerFilePath = "";
        static string LeftBannerFilePath = "";
        private static int currentRowIndex = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("LoginPage.aspx");
            bool[] rightsArray = AccessRights.SetUserAccessRights("CampaignDetail");
            //if (!rightsArray[3])
            //{
            //    viewAll.Visible = false;
            //}

            if (!Page.IsPostBack)
            {
                TopBannerFilePath = "";
                TopBannerFilePath = "";
                txtLinkedIn.Text = "https://www.linkedin.com/company/falconautotech/";
                txtYouTube.Text = "https://www.youtube.com/user/FalconAutotech";
                txtTwitter.Text = "https://twitter.com/falconautotech";
                txtFaceBook.Text = "https://www.facebook.com/falconautotech";
                txtTelephoneNumber.Text = "+91 9711 488606";
                txtFormName.Text = "Contact us";
                txtEmailSubject.Text = "Thank you for Contacting Falcon Autotech";
                txtEmailBody.Text = "Thank you for your interest in the FALCON AUTOTECH."
+ "<br/>If you need any additional information, Please visit our website www.falconautoonline.com or give us a call at +91 9711 488606."
+ "<br/>You can also download the CUBIZON PDF Brochure by clicking here"
+ "<br/>Regards"
+ "<br/>Team Falcon"
+ "<br/>https://www.falconautoonline.com";

                if (!string.IsNullOrEmpty(Request.QueryString["CampaignDetailId"]))
                {

                    LoadData(Request.QueryString["CampaignDetailId"]);
                }


            }

        }





        protected void LoadData(string id)
        {
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand("select * from campaign_detail where Campaign_detail_id=@Campaigndetailid", con);
                cmd.Parameters.AddWithValue("@Campaigndetailid", id);
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    //string productName = "";
                    //if (reader["Campaign_detail_id"] != DBNull.Value)
                    //{
                    //    productName = Validation.RetrieveTextById("Product_Detail", "Product_Name", "Product_Detail_Id", Convert.ToString(reader["Product_Detail_Id"]));
                    //}
                    //string documentType = "";
                    //if (reader["Document_Type_Master_Id"] != DBNull.Value)
                    //{
                    //    documentType = Validation.RetrieveTextById("Document_Type_Master", "Document_Type", "Document_Type_Master_Id", Convert.ToString(reader["Document_Type_Master_Id"]));
                    //}

                    //inputDocumentType.Text = documentType;
                    //inputProductName.Text = productName;
                    inputCategory.Text = Convert.ToString(reader["Category"]);
                    inputTitle.Text = Convert.ToString(reader["Title"]);
                    inputDescription.Text = Convert.ToString(reader["Description"]);
                    inputRemarks.Text = Convert.ToString(reader["Remarks"]);
                    TopBannerFilePath = Convert.ToString(reader["Top_Bannner"]);
                    LeftBannerFilePath = Convert.ToString(reader["Left_Banner"]);
                    if (Convert.ToBoolean(reader["Is_Active_Top_Banner"]))
                    {
                        checkIsActiveTopBanner.Checked = true;
                    }
                    else
                    {
                        checkIsActiveTopBanner.Checked = false;
                    }
                    if (Convert.ToBoolean(reader["Is_Active_Left_Banner"]))
                    {
                        checkIsActiveLeftBanner.Checked = true;
                    }
                    else
                    {
                        checkIsActiveLeftBanner.Checked = false;
                    }
                    if (reader["Text_Below_Contact"] != DBNull.Value)
                    {
                        inputTextBelowContentForm.Text = Convert.ToString(reader["Text_Below_Contact"]);
                    }
                    checkIsActiveDownloadLink.Checked = Convert.ToBoolean(reader["Is_Active_Download_Link"]);
                    inputDownLoadLink.Text = Convert.ToString(reader["DownLoad_Link"]);
                    txtEmailSubject.Text = Convert.ToString(reader["Email_Subject"]);
                    txtEmailBody.Text = Convert.ToString(reader["Email_Body"]);
                    txtFormName.Text = Convert.ToString(reader["Form_Name"]);
                    txtTopDescription.Text = Convert.ToString(reader["Top_Description"]);
                    txtTelephoneNumber.Text = Convert.ToString(reader["Telephone_Number"]);
                    txtLinkedIn.Text = Convert.ToString(reader["LinkedIn"]);
                    txtYouTube.Text = Convert.ToString(reader["YouTube"]);
                    txtTwitter.Text = Convert.ToString(reader["Twitter"]);
                    txtFaceBook.Text = Convert.ToString(reader["FaceBook"]);
                   
                    string ipAddress = File.ReadAllText(Server.MapPath("~/NetworkIPAddress.txt"));
                    if (ipAddress.Length > 0)
                    {
                        lblLinkFormed.Text = "http://" + ipAddress + "/Campaign.aspx?Camp=" + inputCategory.Text.Trim() + "";
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }

        }

        protected void ClearControls()
        {

            inputDownLoadLink.Text = String.Empty;
            inputLeftBanner.Dispose();
            checkIsActiveLeftBanner.Checked = false;
            inputTopBanner.Dispose();
            checkIsActiveTopBanner.Checked = false;
            inputDescription.Text = String.Empty;
            inputTitle.Text = String.Empty;
            inputCategory.Text = String.Empty;
            txtEmailSubject.Text = String.Empty;
            txtEmailBody.Text = String.Empty;
            txtFormName.Text = String.Empty;
            txtTelephoneNumber.Text = String.Empty;
            txtTopDescription.Text = String.Empty;
            txtLinkedIn.Text = String.Empty;
            txtYouTube.Text = String.Empty;
            txtTwitter.Text = String.Empty;
            txtFaceBook.Text = String.Empty;
            inputTextBelowContentForm.Text = String.Empty;
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.Clear();
                    if (!string.IsNullOrEmpty(Request.QueryString["CampaignDetailId"]))
                    {
                        cmd.CommandText = "select Count(Campaign_detail_id) as Count from campaign_detail where Category='" + inputCategory.Text + "' and Campaign_detail_id!=@Campaigndetailid";
                        cmd.Parameters.AddWithValue("@Campaigndetailid", Request.QueryString["CampaignDetailId"]);
                    }
                    else
                    {
                        cmd.CommandText = "select Count(Campaign_detail_id) as Count from campaign_detail where Category='" + inputCategory.Text + "'";
                    }
                    MySqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        int count = Convert.ToInt32(reader["Count"]);
                        if (count > 0)
                        {
                            lblCategory.Text = "Category already exist";
                            lblCategory.ForeColor = System.Drawing.Color.Red;
                            reader.Close();
                            return;
                        }
                        reader.Close();
                    }

                    string path = "";
                    if (inputTopBanner.HasFile)
                    {
                        string fileServerPath = Server.MapPath("Upload/TopBanner");

                        if (!Directory.Exists(fileServerPath))
                        {
                            Directory.CreateDirectory(fileServerPath);
                        }
                        string fileName = Path.GetFileNameWithoutExtension(inputTopBanner.PostedFile.FileName);
                        string fileExtension = Path.GetExtension(inputTopBanner.PostedFile.FileName);
                        string newDocumentName = "";

                        string date = DateTime.Now.Day + "" + DateTime.Now.Month + "" + DateTime.Now.Year + "" + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                        newDocumentName = fileName + "" + date + "" + fileExtension;
                        path = "~/Upload/TopBanner/" + newDocumentName;

                        inputTopBanner.PostedFile.SaveAs(Server.MapPath(path));
                        TopBannerFilePath = path;
                    }

                    if (inputLeftBanner.HasFile)
                    {
                        path = "";
                        string fileServerPath = Server.MapPath("Upload/LeftBanner");

                        if (!Directory.Exists(fileServerPath))
                        {
                            Directory.CreateDirectory(fileServerPath);
                        }
                        string fileName = Path.GetFileNameWithoutExtension(inputLeftBanner.PostedFile.FileName);
                        string fileExtension = Path.GetExtension(inputLeftBanner.PostedFile.FileName);
                        string newDocumentName = "";

                        string date = DateTime.Now.Day + "" + DateTime.Now.Month + "" + DateTime.Now.Year + "" + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                        newDocumentName = fileName + "" + date + "" + fileExtension;
                        path = "~/Upload/LeftBanner/" + newDocumentName;

                        inputLeftBanner.PostedFile.SaveAs(Server.MapPath(path));
                        LeftBannerFilePath = path;
                    }
                    //if (inputTopBanner.HasFile)
                    //{
                    //    string fileServerPath = Server.MapPath("Upload/TopBanner");

                    //    if (!Directory.Exists(fileServerPath))
                    //    {
                    //        Directory.CreateDirectory(fileServerPath);
                    //    }

                    //    string fileName = Path.GetFileNameWithoutExtension(inputTopBanner.PostedFile.FileName);
                    //    string fileExtension = Path.GetExtension(inputTopBanner.PostedFile.FileName);

                    //    inputTopBanner.PostedFile.SaveAs(fileServerPath + "/" + inputTopBanner.PostedFile.FileName);
                    //    TopBannerFilePath = "Upload/TopBanner/" + inputTopBanner.PostedFile.FileName;
                    //}

                    //if (inputLeftBanner.HasFile)
                    //{
                    //    string fileServerPath = Server.MapPath("Upload/LeftBanner");

                    //    if (!Directory.Exists(fileServerPath))
                    //    {
                    //        Directory.CreateDirectory(fileServerPath);
                    //    }

                    //    inputLeftBanner.PostedFile.SaveAs(fileServerPath + "/" + inputLeftBanner.PostedFile.FileName);
                    //    LeftBannerFilePath = "Upload/LeftBanner/" + inputLeftBanner.PostedFile.FileName;
                    //}

                    cmd.Parameters.Clear();
                    if (!string.IsNullOrEmpty(Request.QueryString["CampaignDetailId"]))
                    {
                        cmd = new MySqlCommand("update campaign_detail set Category=@Category,Title=@Title,Description=@Description,Top_Bannner=@TopBannner,Left_Banner=@LeftBanner,DownLoad_Link=@DownLoadLink,Is_Active_Download_Link=@IsActiveDownloadLink, Is_Active_Top_Banner=@IsActiveTopBanner, Is_Active_Left_Banner=@IsActiveLeftBanner, Remarks=@Remarks,Updated_Date=@UpdatedDate,Updated_By=@UpdatedBy,Email_Subject=@EmailSubject,Email_Body=@EmailBody,Form_Name=@FormName,Top_Description=@TopDescription,Telephone_Number=@TelephoneNumber,LinkedIn=@LinkedIn,YouTube=@YouTube,Twitter=@Twitter,FaceBook=@FaceBook,Text_Below_Contact=@TextBelowContact where Campaign_detail_id=@Campaigndetailid", con);
                        cmd.Parameters.AddWithValue("@Campaigndetailid", Request.QueryString["CampaignDetailId"]);
                        cmd.Parameters.AddWithValue("@UpdatedDate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserID"]);
                    }
                    else
                    {
                        cmd = new MySqlCommand("insert into campaign_detail  (Category,Title,Description , Top_Bannner,Left_Banner,DownLoad_Link ,Is_Active_Download_Link, Is_Active_Top_Banner , Is_Active_Left_Banner , Remarks ,Created_Date,Created_By ,Email_Subject ,Email_Body ,Form_Name ,Top_Description ,Telephone_Number ,LinkedIn ,YouTube ,Twitter ,FaceBook,Text_Below_Contact) Values(@Category,@Title,@Description,@TopBannner,@LeftBanner,@DownLoadLink ,@IsActiveDownloadLink, @IsActiveTopBanner , @IsActiveLeftBanner , @Remarks,@CreatedDate,@CreatedBy ,@EmailSubject ,@EmailBody ,@FormName ,@TopDescription ,@TelephoneNumber ,@LinkedIn ,@YouTube ,@Twitter ,@FaceBook,@TextBelowContact)", con);
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["UserID"]);
                    }
                    cmd.Parameters.AddWithValue("@Category", inputCategory.Text);
                    cmd.Parameters.AddWithValue("@Title", inputTitle.Text);
                    cmd.Parameters.AddWithValue("@Description", inputDescription.Text);
                    cmd.Parameters.AddWithValue("@TopBannner", TopBannerFilePath);
                    cmd.Parameters.AddWithValue("@LeftBanner", LeftBannerFilePath);
                    cmd.Parameters.AddWithValue("@DownLoadLink", inputDownLoadLink.Text);
                    cmd.Parameters.AddWithValue("@IsActiveDownloadLink", checkIsActiveDownloadLink.Checked);
                    cmd.Parameters.AddWithValue("@IsActiveTopBanner", checkIsActiveTopBanner.Checked);
                    cmd.Parameters.AddWithValue("@IsActiveLeftBanner", checkIsActiveLeftBanner.Checked);
                    cmd.Parameters.AddWithValue("@Remarks", inputRemarks.Text);
                    cmd.Parameters.AddWithValue("@EmailSubject", txtEmailSubject.Text);
                    cmd.Parameters.AddWithValue("@EmailBody", txtEmailBody.Text);
                    cmd.Parameters.AddWithValue("@FormName", txtFormName.Text);
                    cmd.Parameters.AddWithValue("@TopDescription", txtTopDescription.Text);
                    cmd.Parameters.AddWithValue("@TelephoneNumber", txtTelephoneNumber.Text);
                    cmd.Parameters.AddWithValue("@LinkedIn", txtLinkedIn.Text);
                    cmd.Parameters.AddWithValue("@YouTube", txtYouTube.Text);
                    cmd.Parameters.AddWithValue("@Twitter", txtTwitter.Text);
                    cmd.Parameters.AddWithValue("@FaceBook", txtFaceBook.Text);
                    cmd.Parameters.AddWithValue("@TextBelowContact", inputTextBelowContentForm.Text);
                    cmd.ExecuteNonQuery();
                    lblMessage.Text = "Data Saved Successfully";

                    string ipAddress = File.ReadAllText(Server.MapPath("~/NetworkIPAddress.txt"));
                    if (ipAddress.Length > 0)
                    {
                        lblLinkFormed.Text = "http://" + ipAddress + "/Campaign.aspx?Camp=" + inputCategory.Text.Trim() + "";
                    }
                    if (string.IsNullOrEmpty(Request.QueryString["CampaignDetailId"]))
                    {
                        ClearControls();
                        // Response.Redirect("ProductDocumentAdmin.aspx?PageIndex=" + Request.QueryString["PageIndex"], false);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
        }







        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public static List<string> GetProductName(string text)
        //{
        //    MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
        //    try
        //    {
        //        con.Open();
        //        List<string> allItems = new List<string>();
        //        MySqlCommand cmd = new MySqlCommand();
        //        if (!string.IsNullOrWhiteSpace(text))
        //        {
        //            cmd = new MySqlCommand("select distinct Product_Name from Product_Detail where Product_Name like '%" + text + "%' order by Product_Name ASC", con);
        //        }
        //        else
        //        {
        //            cmd = new MySqlCommand("select distinct Product_Name from Product_Detail order by Product_Name ASC", con);
        //        }
        //        MySqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            string productName = Convert.ToString(reader["Product_Name"]);
        //            allItems.Add(productName);
        //        }
        //        reader.Close();
        //        return allItems;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        return null;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public static List<string> GetDepartmentName(string text)
        //{
        //    MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
        //    try
        //    {
        //        con.Open();
        //        List<string> allItems = new List<string>();
        //        MySqlCommand cmd = new MySqlCommand();
        //        if (!string.IsNullOrWhiteSpace(text))
        //        {
        //            cmd = new MySqlCommand("select distinct Department_Name from Department_Master where Department_Name like '%" + text + "%' order by Department_Name ASC", con);
        //        }
        //        else
        //        {
        //            cmd = new MySqlCommand("select distinct Department_Name from Department_Master order by Department_Name ASC", con);
        //        }
        //        MySqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            string departmentName = Convert.ToString(reader["Department_Name"]);
        //            allItems.Add(departmentName);
        //        }
        //        reader.Close();
        //        return allItems;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        return null;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}



        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public static string GetVersionNumber(string productname, string documenttype)
        //{
        //    MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
        //    try
        //    {
        //        string currentVersion = "";
        //        if (!string.IsNullOrEmpty(productname) && !string.IsNullOrEmpty(documenttype))
        //        {
        //            con.Open();
        //            MySqlCommand cmd = new MySqlCommand("select * from Product_Document where Product_Detail_Id=@ProductDetailId and Document_Type_Master_Id=@DocumentTypeMasterId order by Document_Version Desc limit 1", con);
        //            cmd.Parameters.AddWithValue("@ProductDetailId", Validation.RetrieveIdByText("Product_Detail", "Product_Name", "Product_Detail_Id", productname));
        //            cmd.Parameters.AddWithValue("@DocumentTypeMasterId", Validation.RetrieveIdByText("Document_Type_Master", "Document_Type", "Document_Type_Master_Id", documenttype));
        //            using (MySqlDataReader reader = cmd.ExecuteReader())
        //            {

        //                if (reader.HasRows)
        //                {
        //                    reader.Read();
        //                    string version = Convert.ToString(reader["Document_Version"]);
        //                    string[] splitVersion = Regex.Split(version, "V");
        //                    int newversion = Convert.ToInt32(splitVersion[1]) + 1;
        //                    currentVersion = "V" + newversion;
        //                }
        //                else
        //                {
        //                    currentVersion = "V" + 1;
        //                }
        //            }

        //        }
        //        return currentVersion;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        return null;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public static List<string> GetEmployeeName(string text)
        //{
        //    MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
        //    try
        //    {
        //        con.Open();
        //        List<string> allItems = new List<string>();
        //        MySqlCommand cmd = new MySqlCommand();
        //        if (!string.IsNullOrWhiteSpace(text))
        //        {
        //            cmd = new MySqlCommand("select User_Name from Engineer_Detail where Engineer_Name like '%" + text + "%' order by User_Name ASC", con);
        //        }
        //        else
        //        {
        //            cmd = new MySqlCommand("select User_Name from Engineer_Detail order by User_Name ASC", con);
        //        }
        //        MySqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {

        //            string result = Convert.ToString(reader["User_Name"]);
        //            allItems.Add(result);
        //        }
        //        reader.Close();
        //        return allItems;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        return null;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}


        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public static List<string> GetDocumentType(string text)
        //{
        //    MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
        //    try
        //    {
        //        con.Open();
        //        List<string> allItems = new List<string>();
        //        MySqlCommand cmd = new MySqlCommand();
        //        if (!string.IsNullOrWhiteSpace(text))
        //        {
        //            cmd = new MySqlCommand("select distinct Document_Type from Document_Type_Master where Document_Type like '%" + text + "%' order by Document_Type ASC", con);
        //        }
        //        else
        //        {
        //            cmd = new MySqlCommand("select distinct Document_Type from Document_Type_Master order by Document_Type ASC", con);
        //        }
        //        MySqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            string documentType = Convert.ToString(reader["Document_Type"]);
        //            allItems.Add(documentType);
        //        }
        //        reader.Close();
        //        return allItems;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        return null;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}




    }
}