﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Text;
using System.IO;
using System.Net;
using System.Web.Security;
using System.Security.Cryptography;
using System.Data;
using System.DirectoryServices;
using System.Web.Services;
using System.Web.Script.Services;

namespace CampaignManagement
{
    public partial class LoginPage : System.Web.UI.Page
    {
        Program obj = new Program();
        public static List<string> ipaddresslist = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                StreamReader reader = new StreamReader(Server.MapPath("~/ConfigInfo.txt"));
                string content = reader.ReadToEnd();
                string[] getUserCredentials = content.Split(';');
                if (getUserCredentials[0] != null)
                {
                    MySqlConnectionStringBuilder connectionstring = new MySqlConnectionStringBuilder();
                    connectionstring.Server = getUserCredentials[0];
                    connectionstring.Database = getUserCredentials[1];
                    connectionstring.MinimumPoolSize = 50;
                    connectionstring.MaximumPoolSize = 1000;
                    connectionstring.ConnectionTimeout = 60;
                    connectionstring.UserID = getUserCredentials[2];
                    if (getUserCredentials[3] != null)
                    {
                        connectionstring.Password = getUserCredentials[3];
                    }
                    InitializeConnection.connection = connectionstring.ToString();
                }
            }
        }



        public static string GetMD5(string text)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;
            StringBuilder str = new StringBuilder();

            for (int i = 1; i < result.Length; i++)
            {
                str.Append(result[i].ToString("x2"));
            }

            return str.ToString();
        }



        public string GetIP()
        {
            string Str = "";
            Str = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Str);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            MySqlConnection connec = new MySqlConnection(InitializeConnection.connection);
            try
            {
                connec.Open();
                MySqlCommand cmd = new MySqlCommand("select * from user_login where User_Name=@UserName and User_Password=@Password", connec);
                cmd.Parameters.AddWithValue("@UserName", inputUserID.Text);
                cmd.Parameters.AddWithValue("@Password", inputPassword.Text);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        Session["UserID"] = Convert.ToString(reader["User_Name"]);
                        Session["Role"] = Convert.ToString(reader["Role"]);
                        HttpContext.Current.Response.Redirect("Dashboard.aspx",false);
                    }
                    else
                    {
                        lblMessage.Text = "Invalid User ID or Password";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                connec.Close();
            }
        }


    }
}