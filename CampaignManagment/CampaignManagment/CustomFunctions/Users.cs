﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.DirectoryServices;
using System.IO;
using MySql.Data.MySqlClient;

namespace CampaignManagement
{
    //This class is used for retriving Users from LDAP and storing it into UserList
    public class Users
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string EmployeeCode { get; set; }
        public string Department { get; set; }


        public static List<Users> LoadUsersList()
        {
            List<Users> lstADUsers = new List<Users>();
            try
            {
                string DomainPath = "LDAP://192.168.1.254/CN=USERS,DC=falconautoonline,DC=com";
                DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);//, "maneesh_bhatt", "Falcon@123"
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.Filter = "(&(objectClass=user)(objectCategory=person))";
                search.PropertiesToLoad.Add("samaccountname");
                search.PropertiesToLoad.Add("mail");
                search.PropertiesToLoad.Add("usergroup");
                search.PropertiesToLoad.Add("displayname");
                search.PropertiesToLoad.Add("description");
                search.PropertiesToLoad.Add("department");

                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();
                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        string UserNameEmailString = string.Empty;
                        result = resultCol[counter];
                        if (result.Properties.Contains("samaccountname") &&
                                 result.Properties.Contains("mail") &&
                            result.Properties.Contains("displayname") && result.Properties.Contains("description") && result.Properties.Contains("department"))
                        {
                            Users objSurveyUsers = new Users();
                            objSurveyUsers.Email = (String)result.Properties["mail"][0] +
                              "^" + (String)result.Properties["displayname"][0];
                            objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                            objSurveyUsers.DisplayName = (String)result.Properties["displayname"][0];
                            if (result.Properties.Contains("description"))
                                objSurveyUsers.EmployeeCode = (String)result.Properties["description"][0];
                            if (result.Properties.Contains("department"))
                                objSurveyUsers.Department = (String)result.Properties["department"][0];
                            lstADUsers.Add(objSurveyUsers);
                        }
                    }
                }
                lstADUsers = lstADUsers.OrderBy(x => x.UserName).ToList();
                return lstADUsers;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return lstADUsers;
            }

        }


        public static void UpdateLDAPData()
        {
            List<Users> lstADUsers = new List<Users>();
            try
            {
                string DomainPath = "LDAP://192.168.1.254/CN=USERS,DC=falconautoonline,DC=com";
                DirectoryEntry searchRoot = new DirectoryEntry(DomainPath, "maneesh_bhatt", "Falcon@123");//, "maneesh_bhatt", "Falcon@123"
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.Filter = "(&(objectClass=user)(objectCategory=person))";
                search.PropertiesToLoad.Add("samaccountname");
                search.PropertiesToLoad.Add("mail");
                search.PropertiesToLoad.Add("usergroup");
                search.PropertiesToLoad.Add("displayname");
                search.PropertiesToLoad.Add("displayname");
                //first name
                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();
                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        string UserNameEmailString = string.Empty;
                        result = resultCol[counter];
                        if (result.Properties.Contains("samaccountname") &&
                                 result.Properties.Contains("mail") &&
                            result.Properties.Contains("displayname"))
                        {
                            Users objSurveyUsers = new Users();
                            objSurveyUsers.Email = (String)result.Properties["mail"][0];
                            objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                            objSurveyUsers.DisplayName = (String)result.Properties["displayname"][0];
                            lstADUsers.Add(objSurveyUsers);

                            MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
                            con.Open();
                            MySqlCommand cmd = new MySqlCommand();
                            cmd.Connection = con;
                            bool recordExist = false;
                            cmd.Parameters.Clear();
                            cmd.CommandText = "select * from Engineer_Detail where User_Name=@UserName";
                            cmd.Parameters.AddWithValue("@UserName", objSurveyUsers.UserName);
                            using (MySqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    recordExist = true;
                                }
                            }

                            if (!recordExist)
                            {
                                cmd.Parameters.Clear();
                                cmd.CommandText = "insert into Engineer_Detail(Engineer_Name,User_Name,Email_Address) Values(@EngineerName,@UserName, @EmailAddress)";
                                cmd.Parameters.AddWithValue("@EngineerName", objSurveyUsers.DisplayName);
                                cmd.Parameters.AddWithValue("@UserName", objSurveyUsers.UserName);
                                cmd.Parameters.AddWithValue("@EmailAddress", objSurveyUsers.Email);
                                cmd.ExecuteNonQuery();
                            }
                        }


                    }
                }
                lstADUsers = lstADUsers.OrderBy(x => x.UserName).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}