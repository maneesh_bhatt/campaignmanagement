﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Net.Mail;

namespace CampaignManagment
{
    public partial class ContactForm : System.Web.UI.Page
    {
        public static MySqlConnection con = new MySqlConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                StreamReader rdr = new StreamReader(Server.MapPath("~/ConfigInfo.txt"));
                string content = rdr.ReadToEnd();
                string[] getUserCredentials = content.Split(';');
                if (getUserCredentials[0] != null)
                {
                    MySqlConnectionStringBuilder connectionstring = new MySqlConnectionStringBuilder();
                    connectionstring.Server = getUserCredentials[0];
                    connectionstring.Database = getUserCredentials[1];
                    connectionstring.MinimumPoolSize = 50;
                    connectionstring.MaximumPoolSize = 1000;
                    connectionstring.ConnectionTimeout = 60;
                    connectionstring.UserID = getUserCredentials[2];
                    if (getUserCredentials[3] != null)
                    {
                        connectionstring.Password = getUserCredentials[3];
                    }
                    con.ConnectionString = connectionstring.ToString();
                }

                
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "ClearMessage()", true);
            lblMessage.Text = "";
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string ipaddress = string.Empty;
                string cityName = string.Empty;

                try
                {
                    ipaddress = Server.HtmlEncode(Request.UserHostAddress);
                    if (ipaddress != "")
                    {
                        string url = "http://freegeoip.net/json/" + ipaddress.ToString();
                        WebClient client = new WebClient();
                        string jsonstring = client.DownloadString(url);
                        dynamic dynObj = JsonConvert.DeserializeObject(jsonstring);
                        cityName = dynObj.city + "," + dynObj.country_name;
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                con.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "insert into campaign_customer(Category,Name ,Email ,Mobile_Number ,Company ,Comments ,IP_Address ,Location ,Created_Date) values(@Category,@Name ,@Email ,@MobileNumber ,@Company ,@Comments ,@IPAddress ,@Location ,@CreatedDate)";
                cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@MobileNumber", txtMobileNumber.Text.Trim());
                cmd.Parameters.AddWithValue("@Company", txtCompany.Text.Trim());
                cmd.Parameters.AddWithValue("@Comments", txtComments.Text.Trim());
                cmd.Parameters.AddWithValue("@Category", Request.QueryString["Camp"]);
                cmd.Parameters.AddWithValue("@IPAddress", ipaddress);
                cmd.Parameters.AddWithValue("@Location", cityName);
                cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                cmd.ExecuteNonQuery();


                SendEmail(txtEmail.Text);
                MessageAndClearControls();
                


            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }
        }


        protected void SendEmail(string VisitorEmailId)
        {
            
            try
            {

                
                    SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                    MailMessage newmsg = new MailMessage();
                    newmsg.From = new MailAddress("reminder@falconautoonline.com");
                    newmsg.To.Add(VisitorEmailId);
                    newmsg.Subject = "Thanks for Contacting Us";
                    string content = "Hi Admin test,<br/><br/>"
                        +"Thanks for writing to us. <br/>"
                        +"We would review your request and would get back to you shortly.<br/><br/>"
                        +"Regards<br/>"
                        +"Team Falcon";
                    
                    newmsg.Body = content;

                    SmtpServer.TargetName = "STARTTLS/smtp.office365.com";
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.Port = 587;
                    newmsg.IsBodyHtml = true;
                    SmtpServer.EnableSsl = true;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("reminder@falconautoonline.com", "Falcon@FAPL@2016");
                    SmtpServer.Send(newmsg);
                
            }
            catch (Exception ex)
            {
                ex.ToString();

            }
            
        }
        
        protected void MessageAndClearControls()
        {
            lblMessage.Text = "Data submitted successfully";
            txtName.Text = "";
            txtEmail.Text = "";
            txtMobileNumber.Text = "";
            txtCompany.Text = "";
            txtComments.Text = "";
        }
    }
}