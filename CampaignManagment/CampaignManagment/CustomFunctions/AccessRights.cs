﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using CampaignManagement;

namespace  CustomFunctions
{
    public class AccessRights
    {

        public static  bool [] SetUserAccessRights(string pageName)
        {
            bool[] rights = new bool[5];

            MySqlConnection con = new MySqlConnection(InitializeConnection.connection);
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand("select * from Menu_Rights where Menu_Master_Id in(select Menu_Master_Id from Menu_Master where Menu_Name=@MenuName) and User_Name=@UserName", con);
                cmd.Parameters.AddWithValue("@MenuName", pageName);
                cmd.Parameters.AddWithValue("@UserName", Convert.ToString(HttpContext.Current.Session["UserID"]));
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    rights[0] = Convert.ToBoolean(reader["Add_Rights"]);
                    rights[1] = Convert.ToBoolean(reader["Edit_Rights"]);
                    rights[2] = Convert.ToBoolean(reader["Delete_Rights"]);
                    rights[3] = Convert.ToBoolean(reader["View_Rights"]);
                    rights[4] = Convert.ToBoolean(reader["Download_Rights"]);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                con.Close();
            }
            return rights;
        }
    }
}